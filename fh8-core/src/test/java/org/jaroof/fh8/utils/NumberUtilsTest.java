package org.jaroof.fh8.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumberUtilsTest {

    @Test
    public void roundDoubleTest() {
        double value = NumberUtils.roundDouble(123456.123412341234, 4);
        assertEquals(123456.1234, value);
        assertNotEquals(123456.123, value);
        assertNotEquals(123456.12341, value);
        assertNotEquals(6542.654, value);
        assertNotEquals(0, value);
    }

}