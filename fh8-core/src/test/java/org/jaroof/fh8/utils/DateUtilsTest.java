package org.jaroof.fh8.utils;

import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class DateUtilsTest {
    @Test
    public void studentDate() {
        Calendar testCal = Calendar.getInstance();
        testCal.set(2005, 02, 05);
        Date testDate = testCal.getTime();
        Date studentDate = DateUtils.studentDate();
        assertTrue(studentDate.before(testDate));
    }

    @Test
    public void seniorDate() {
        Calendar testCal = Calendar.getInstance();
        testCal.set(1920, 02, 05);
        Date testDate = testCal.getTime();
        Date seniorDate = DateUtils.seniorDate();
        assertTrue(seniorDate.after(testDate));
    }
}