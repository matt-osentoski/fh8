package org.jaroof.fh8.dto;

import lombok.Data;

@Data
public class VirtualTime {
    private long epochTime;
}
