package org.jaroof.fh8.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreType;
import lombok.Data;

@Data
@JsonIgnoreType
public class Success {
    private boolean value;
}
