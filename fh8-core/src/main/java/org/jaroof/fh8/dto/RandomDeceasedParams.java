package org.jaroof.fh8.dto;

import lombok.Data;

@Data
public class RandomDeceasedParams {
    private Integer age;
    private String sex;
    private Double modifier;
}
