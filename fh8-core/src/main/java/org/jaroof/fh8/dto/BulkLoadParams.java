package org.jaroof.fh8.dto;

import lombok.Data;

@Data
public class BulkLoadParams {
    private int zipcode;
    private int radius;
    private int count;
}
