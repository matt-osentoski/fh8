package org.jaroof.fh8.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class NumberUtils {

    /**
     * Rounds a double using the HALF_UP rounding mode to a certain number of decimal places.
     * @param value The double value to round
     * @param decimalPlaces The number of decimal places to round to
     * @return
     */
    public static final double roundDouble(double value, int decimalPlaces) {
        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(decimalPlaces, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
