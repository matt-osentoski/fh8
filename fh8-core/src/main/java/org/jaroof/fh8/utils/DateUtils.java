package org.jaroof.fh8.utils;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    public static final Date studentDate() {
        Calendar numberFour = Calendar.getInstance();
        numberFour.add(Calendar.YEAR, -18);
        return numberFour.getTime();
    }

    public static final Date seniorDate() {
        Calendar numberFour = Calendar.getInstance();
        numberFour.add(Calendar.YEAR, -62);
        return numberFour.getTime();
    }

    public static int calculateAge(Date birthDate, Date currentDate) {
        return DateUtils.calculateAge(convertToLocalDateViaInstant(birthDate),
                convertToLocalDateViaInstant(currentDate));
    }

    public static int calculateAge(LocalDate birthDate, LocalDate currentDate) {
        if ((birthDate != null) && (currentDate != null)) {
            return Period.between(birthDate, currentDate).getYears();
        } else {
            return 0;
        }
    }

    public static LocalDate convertToLocalDateViaInstant(Date dateToConvert) {
        return dateToConvert
                .toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }
}
