package org.jaroof.fh8.utils;

import org.hl7.fhir.r4.model.*;
import org.jaroof.fh8.Constants;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class FhirUtils {

    /**
     * Returns the first name of a patient assuming a simple name (first / last name)
     * @param patient
     * @return
     */
    public static String getFirstName(Patient patient) {
        List<HumanName> names = patient.getName();
        StringType firstName = names.stream()
                .flatMap(n -> n.getGiven().stream())
                .findFirst()
                .orElse(null);

        return firstName.getValue();
    }

    /**
     * Returns the last name of a patient assuming a simple name (first / last name)
     * @param patient
     * @return
     */
    public static String getLastName(Patient patient) {
        List<HumanName> names = patient.getName();
        HumanName humanName = names.stream()
                .findFirst()
                .orElse(null);

        return humanName.getFamily();
    }


    /**
     * Finds a UUID identifier value from the identity array with a FHIR resource.
     * @param identifiers
     * @return UUID identifier value
     */
    public static String getUuidFromIdentifiers(List<Identifier> identifiers) {
        Identifier identifier = identifiers.stream()
                .filter(i -> i.getSystem().equals(Constants.ID_SYSTEM))
                .findFirst()
                .orElse(null);
        return identifier.getValue();
    }

    /**
     * Creates a Identifier List to be used in a FHIR resource with a single Identifier with a UUID
     * @return List of Identifiers
     */
    public static List<Identifier> createSingleUuidIdentifier() {
        List<Identifier> ids = new ArrayList<>();
        Identifier id = new Identifier();
        id.setSystem(Constants.ID_SYSTEM);
        id.setValue(UUID.randomUUID().toString());
        ids.add(id);
        return ids;
    }

    /**
     * Creates Meta section to be used in a FHIR resource with a version of 1 and last updated with the current date.
     * @return FHIR Meta Section
     */
    public static Meta createBaseMeta() {
        Meta meta = new Meta();
        meta.setVersionId("1");
        meta.setLastUpdated(new Date());
        return meta;
    }

    /**
     * Creates a single CodeableConcept to be used in a FHIR resource
     * @param system The name of the system to use
     * @param code The code value
     * @param display The display name of the code
     * @return CodeablConcept
     */
    public static CodeableConcept createSingleCodeableConcept(String system, String code, String display) {
        Coding coding = new Coding();
        coding.setSystem(system);
        coding.setCode(code);
        coding.setDisplay(display);
        List<Coding> codings = new ArrayList<>();
        codings.add(coding);
        CodeableConcept codeableConcept = new CodeableConcept();
        codeableConcept.setCoding(codings);
        return codeableConcept;
    }
}
