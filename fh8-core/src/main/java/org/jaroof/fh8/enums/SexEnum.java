package org.jaroof.fh8.enums;

/**
 * Enumeration for sex using the FHIR
 */
public enum SexEnum {
    MALE ("male"),
    FEMALE ("female"),
    UNKNOWN ("unknown");

    private final String value;

    SexEnum(String sex) {
        value = sex;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.getValue();
    }
}
