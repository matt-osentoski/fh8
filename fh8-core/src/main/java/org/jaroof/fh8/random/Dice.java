package org.jaroof.fh8.random;

import lombok.Data;

@Data
public class Dice {
    private int size;
    private int criticalHit;
}
