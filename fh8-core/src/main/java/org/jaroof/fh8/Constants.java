package org.jaroof.fh8;

public class Constants {

    public final static String ID_SYSTEM = "urn:ietf:rfc:3986";
    public final static String FULL_URL_SYSTEM = "urn:uuid:";

    public final static String CODING_SYSTEM_SNOMED = "http://snomed.info/sct";
    public final static String CODING_SYSTEM_OBSERVATION_VALUE_HL7_V3 =
            "http://terminology.hl7.org/CodeSystem/v3-ObservationValue";
    public final static int MAX_AGE = 105;
    public final static String SNOMED_CODE_OCCUPATION = "24933012";
    public final static String HL7_V3_CODE_SALARY = "ECH";

    public final static int DAYS_IN_YEAR = 365;
}
