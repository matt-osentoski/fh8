package org.jaroof.fh8.dmn.functions;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

public class DmnDate {

    public static Date getRandomBirthdate(int generatedAge, LocalDate currentDate) {

        int currentYear = currentDate.getYear();
        int birthYear = currentYear - generatedAge;
        int month = 12;
        int dayOfMonth = 31;
        if (birthYear == currentYear) {
            month = currentDate.getMonthValue();
            dayOfMonth = currentDate.getDayOfMonth();
        }
        int minDay = (int) LocalDate.of(birthYear, 1, 1).toEpochDay();
        int maxDay = (int) LocalDate.of(birthYear, 12, 31).toEpochDay();
        long randomDay = minDay + ThreadLocalRandom.current().nextInt(maxDay - minDay);

        LocalDate randomBirthDate = LocalDate.ofEpochDay(randomDay);

        // Yow... rough conversion..
        Date birthDate = Date.from(randomBirthDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        return birthDate;
    }
}
