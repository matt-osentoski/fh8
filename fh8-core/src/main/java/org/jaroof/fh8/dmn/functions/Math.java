package org.jaroof.fh8.dmn.functions;

import java.util.concurrent.ThreadLocalRandom;

public class Math {

    public static int randomIntRange(int startRange, int endRange) {
        int randomNum = ThreadLocalRandom.current().nextInt(startRange, endRange);
        return randomNum;
    }
}
