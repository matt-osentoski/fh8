package org.jaroof.fh8.service;

public interface EventService {
    void processEvent(String jsonPerson);
}
