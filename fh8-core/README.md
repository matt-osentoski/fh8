# fh8 Core
This module contains common code and utilities used across all modules, with the
exception of Entity classes. Entity classes are stored in the `fh8-domain` module.

## Manually build the module
This module is normally build from the parent POM, but can be built individually.
```
mvn clean install
```