package org.jaroof.fh8.db;

import ca.uhn.fhir.context.FhirContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {

    @Bean
    public FhirContext fhirContext() {
        return FhirContext.forR4();
    }

//TODO Uncomment for bulk processing TBD
//
//    @Autowired
//    private ConsumerFactory kafkaConsumerFactory;
//
//    /**
//     * Batch listener to process multiple Kafka messages at once.
//     * @return
//     */
//    @Bean
//    public KafkaListenerContainerFactory<?> batchFactory() {
//        ConcurrentKafkaListenerContainerFactory<Integer, String> factory =
//                new ConcurrentKafkaListenerContainerFactory<>();
//        factory.setConsumerFactory(kafkaConsumerFactory);
//        factory.setBatchListener(true);  // <--- This sets the factory to batch mode
//        return factory;
//    }
}
