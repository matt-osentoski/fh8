package org.jaroof.fh8.db.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class KafkaFhirBundleService {

    private final PersonPersistenceService personPersistenceService;

    public KafkaFhirBundleService(PersonPersistenceService personPersistenceService) {
        this.personPersistenceService =personPersistenceService;
    }

    @KafkaListener(topics = "#{'${kafka.topic.insert.person}'.split(',')}")
    public void consume(String message) {
        log.info(String.format("$$ -> Consumed Message -> %s",message));
        personPersistenceService.insertPerson(message);
    }
}
