# fh8-db-insert-persistence-svc
This module is a Microservice that consumes FHIR Patient (and related) resources from a Kafka topic then
inserts them into an RDBMS.  All FHIR resources are assumed to be packaged in a bundle
during ingestion.

>(NOTE: This service performs bulk insert operations into a FHIR server. This service
>is not meant to be idempotent.  For updates/idempotent operations a different service should be used)

The FHIR server used by the fh8 application should be considered the 'source of truth', while the database populated from this service
should be treated as more of a transient 'view' of the data, for faster and more efficient retrieval
than is normally possible from a FHIR API.

## Application Flow
- Consume a FHIR Bundle from a Kafka topic.
- Extract individual FHIR resources from the bundle
- INSERT/UPDATE records into a database

## Manually build the module
This module is normally build from the parent POM, but can be built individually.
```
mvn clean install
```

