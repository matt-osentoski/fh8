# fh8-life-event-svc
This module is a Microservice that consumes population messages from a kafka topic and performs life event processing.
Life events in this case, are related to births/deaths.  Any changes, are placed onto the update person topic as a FHIR
object to be consumed by a downstream service.

## Application Flow
- Consume population JSON messages from a Kafka topic. Each message is a denormalized object of a person.
- Perform operations to determine if a birth will result, or if a death event will occur. (LifeEventService)
- Query the FHIR server and create/update the FHIR object if appropriate.
- Using a Kafka producer, place the person FHIR objects onto the topic, if an event occurred with this iteration.

## Manually build the module
This module is normally build from the parent POM, but can be built individually.
```
mvn clean install
```

