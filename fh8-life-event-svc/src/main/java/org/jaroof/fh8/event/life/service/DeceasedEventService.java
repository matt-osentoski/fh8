package org.jaroof.fh8.event.life.service;

import lombok.extern.slf4j.Slf4j;
import org.jaroof.fh8.domain.population.Person;
import org.jaroof.fh8.dto.Success;
import org.jaroof.fh8.event.life.model.DmnDeceasedEventReqBody;
import org.jaroof.fh8.event.life.model.DmnDeceasedEventResponse;
import org.jaroof.fh8.utils.DateUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * Determine if a person is deceased during this time period.
 */
@Service
@Slf4j
public class DeceasedEventService {

    private String uri;
    private RestTemplate restTemplate;
    private HttpHeaders applicationJsonHeaders;

    public DeceasedEventService(@Value("${is.deceased.service.url}") String uri,
                                RestTemplate restTemplate,
                                HttpHeaders applicationJsonHeaders) {
        this.uri = uri;
        this.restTemplate = restTemplate;
        this.applicationJsonHeaders = applicationJsonHeaders;
    }

    /**
     * Calls a REST service to determine if a patient is deceased during this time period
     *
     * TODO: Add another lookup (maybe further upstream that pulls FHIR information and looks for health
     * information.  Then apply an algorithm to adjust the deceased modifier based on health issues, etc.)
     *
     * @param person The person object
     * @return
     */
    public boolean isDeceased(Person person) {
        boolean isPersonDeceased =false;
        DmnDeceasedEventResponse retVal = restTemplate.postForObject(uri, this.createRequestBody(person), DmnDeceasedEventResponse.class);
        log.debug("DMN Response: " + retVal.toString());
        isPersonDeceased = retVal.isDeceased();
        return isPersonDeceased;
    }

    /**
     * Creates the RESTful POST body.
     * @param person
     * @return
     */
    protected DmnDeceasedEventReqBody createRequestBody(Person person) {
        DmnDeceasedEventReqBody reqBody = new DmnDeceasedEventReqBody();
        int age = DateUtils.calculateAge(person.getBirthdate(), person.getVirtualDate());
        reqBody.setAge(age);
        reqBody.setSex(person.getGender());
        reqBody.setModifier(2);
        log.debug("DMN params: " + reqBody.toString() );
        return reqBody;
    }

    /**
     * @deprecated Used for an older Spring Boot service.  This service now uses a quarkus/Kogito endpoint that's a POST
     * <br><br>
     * Generates URL parameters as a Map for the RESt call.
     * @param person
     * @return
     */
    protected Map<String, String> createParamMap(Person person) {
        Map<String, String> params = new HashMap<>();
        // Use the virtual date to calculate age.
        int age = DateUtils.calculateAge(person.getBirthdate(), person.getVirtualDate());
        params.put("age", Integer.toString(age));
        params.put("sex", person.getGender());
        //TODO: adjust the multiplier based on health and other factors.
        //NOTE: using '1' seems to produce a larger than expected number of deceased events.  Trying '2' instead
        params.put("modifier", "2");
        return params;
    }
}
