package org.jaroof.fh8.event.life.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class KafkaConsumerPopulationService {

    private KafkaProducerPersonService kafkaProducerPersonService;
    private LifeEventService lifeEventService;

    public KafkaConsumerPopulationService(KafkaProducerPersonService kafkaProducerPersonService,
                                          LifeEventService lifeEventService) {
        this.kafkaProducerPersonService = kafkaProducerPersonService;
        this.lifeEventService = lifeEventService;
    }

    @KafkaListener(topics = "#{'${kafka.topic.population.person}'.split(',')}")
    public void consume(String message) {
        log.debug(String.format("$$ -> Consumed Message -> %s",message));
        lifeEventService.processEvent(message);
    }

}
