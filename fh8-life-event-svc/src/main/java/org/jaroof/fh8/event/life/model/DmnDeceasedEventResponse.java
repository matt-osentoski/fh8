package org.jaroof.fh8.event.life.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

@Data
@JsonIgnoreType
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class DmnDeceasedEventResponse {
    @JsonProperty("isDeceased")
    private boolean isDeceased;
}
