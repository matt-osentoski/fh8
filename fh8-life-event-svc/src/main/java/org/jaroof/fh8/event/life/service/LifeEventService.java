package org.jaroof.fh8.event.life.service;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.DateTimeType;
import org.hl7.fhir.r4.model.Patient;
import org.jaroof.fh8.Constants;
import org.jaroof.fh8.domain.population.Person;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;

/**
 * This class processes all life events for a particular person per period of time.
 */
@Service
@Slf4j
public class LifeEventService implements org.jaroof.fh8.service.EventService {

    private KafkaProducerPersonService kafkaProducerPersonService;
    private DeceasedEventService deceasedEventService;
    private FhirContext fhirContext;
    private String fhirBaseUrl;

    public LifeEventService(KafkaProducerPersonService kafkaProducerPersonService,
                            DeceasedEventService deceasedEventService, FhirContext fhirContext,
                            @Value("${fhir.server.base.url}") String fhirBaseUrl) {
        this.kafkaProducerPersonService = kafkaProducerPersonService;
        this.deceasedEventService = deceasedEventService;
        this.fhirContext = fhirContext;
        this.fhirBaseUrl = fhirBaseUrl;
    }

    /**
     * Process all Life events. <br><br>
     * @param jsonPerson
     */
    @Override
    public void processEvent(String jsonPerson) {
        Person person = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            person = objectMapper.readValue(jsonPerson, Person.class);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }

        String jsonBundle = updateLifeEventsJson(person);
        if (jsonBundle != null) {
            log.debug("processing: " + jsonBundle);
            kafkaProducerPersonService.produceAsyncMessage(jsonBundle);
        } else {
            log.debug("No processing occurred for Patient: " + person.getPatientUuid());
        }
    }

    /**
     * Returns a JSON FHIR Bundle of a person with a series of life events. If no changes have occurred,
     * return null.
     * @param person
     * @return
     */
    protected String updateLifeEventsJson(Person person) {
        String bundleJson = null;
        if (person != null) {
            Bundle bundle = updateLifeEvents(person);
            if (bundle.getEntry() != null && bundle.getEntry().size() > 0) {
                bundleJson = fhirContext.newJsonParser().setPrettyPrint(false).encodeResourceToString(bundle);
            }
        }
        return bundleJson;
    }

    /**
     * Updates a FHIR objects of a person with a series of life events. These FHIR objects are
     * wrapped in a FHIR Bundle
     * <br><br>
     * To avoid excessive messages in kafka, Events that are triggered will add 'Entries' to the
     * FHIR Bundle resource.  Each  event should only add entries if an event occured.  If
     * the Bundle resource containers zero entries, then the bundle will not be sent to Kafka for processing further
     * downstream.
     * @param person
     * @return
     */
    protected Bundle updateLifeEvents(Person person) {
        Bundle bundle = new Bundle();
        bundle.setType(Bundle.BundleType.BATCH);

        // Process Life events
        // NOTE: The bundle object is mutated
        processDeceasedEvent(person, bundle);

        // TODO: Add other life events here, adding to/updating the Bundle resource 'Entry' elements as needed

        return bundle;
    }

    /**
     * Process a deceased event by updating the FHIR Patient's deceased status
     * @param person Population Person Object
     * @param bundle
     */
    protected void processDeceasedEvent(Person person, Bundle bundle) {
        boolean isDeceased = deceasedEventService.isDeceased(person);
        if (isDeceased) {
            // Retrieve the latest FHIR Patient object
            Patient patient = getPatient(Constants.ID_SYSTEM, person.getPatientUuid());
            // Update deceased properties
            patient.getMeta().setLastUpdated(person.getVirtualDate());
            DateTimeType deceasedDate = new DateTimeType(person.getVirtualDate());
            patient.setDeceased(deceasedDate);
            Bundle.BundleEntryComponent entry = bundle.addEntry();
            entry.setResource(patient);
        }
    }

    /**
     * Returns a FHIR Patient object by identifier obtained in he person object.
     * @param patientIdentifierSystem The System value as part of the Patient identifier.
     * @param patientIdentifier The Patient Identifier used to Identify the Patient
     * @return
     */
    protected Patient getPatient(String patientIdentifierSystem, String patientIdentifier) {
        IGenericClient client = fhirContext.newRestfulGenericClient(fhirBaseUrl);
        Bundle patientBundle = client
                .search()
                .forResource(Patient.class)
                .where(Patient.IDENTIFIER.exactly()
                        .systemAndValues(patientIdentifierSystem, patientIdentifier))
                .returnBundle(Bundle.class)
                .execute();
        Optional<Bundle.BundleEntryComponent> first = patientBundle.getEntry().stream().findFirst();
        Patient patient = (Patient) first.get().getResource();
        return patient;
    }
}
