package org.jaroof.fh8.event.life.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@JsonIgnoreType
public class DmnDeceasedEventReqBody {
    private int age;
    private String sex;
    private double modifier;
}
