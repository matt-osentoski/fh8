package org.jaroof.fh8.event.life.service;

import ca.uhn.fhir.context.FhirContext;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Patient;
import org.jaroof.fh8.Constants;
import org.jaroof.fh8.domain.population.Person;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class LifeEventServiceTest {

    @Mock
    private DeceasedEventService deceasedEventService;

    @Spy
    @InjectMocks
    private LifeEventService lifeEventService;

    @Test
    public void processDeceasedEvent() {
        Bundle bundle = new Bundle();
        Person person = new Person();
        person.setPatientUuid(UUID.randomUUID().toString());
        person.setVirtualDate(new Date());
        Patient patient = new Patient();
        assertNull(patient.getDeceasedDateTimeType().getValue());
        when(deceasedEventService.isDeceased(person)).thenReturn(true);
        // NOTE: we're mocking a method within the InjectMocks object, so we have to do things a little different
        // using 'doReturn' instead of 'thenReturn', like the example above.
        // NOTE2: In with this mock, you wrap the object reference in parenthesis with the method outside.
        // For example: 'when(lifeEventService).getPatient' instead of the normal mock example above, which is:
        // 'when(deceasedEventService.isDeceased(person))'
        doReturn(patient).when(lifeEventService).getPatient(Constants.ID_SYSTEM, person.getPatientUuid());

        lifeEventService.processDeceasedEvent(person, bundle);
        String fhirType = patient.getDeceased().fhirType();

        assertEquals("dateTime", fhirType);
        assertTrue(patient.getDeceased().hasType("dateTime"));
        assertTrue(bundle.getEntry().size() > 0);
        assertNotNull(patient.getDeceasedDateTimeType().getValue());
    }
}