# fh8
fh8 (pronounced `fate`) is a Monte Carlo simulation that uses FHIR resources.  The concept is to model interactions
in healthcare and potentially financial scenarios.

## Repository
https://bitbucket.org/matt-osentoski/fh8/src/master/

## Build all modules (including docker images)
```mvn clean install```

## Deployments

## Topics

### Alter /Add paritions to a topic
To scale out a topic and scale pod that consume from kafka, the partitions must be increased
to match the pod count. 

>(NOTE: Kafka does not automatically auto-rebalance partitions, at this time. As new messages
>are added, the topic paritions will populate)
```
./kafka-topics.sh --bootstrap-server localhost:9092 --topic fh8.population.person.json --alter --partitions 10
```
