package org.jaroof.fh8.db.service;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import lombok.extern.slf4j.Slf4j;
import org.hl7.fhir.r4.model.*;
import org.jaroof.fh8.db.repository.PersonRepository;
import org.jaroof.fh8.domain.population.Person;
import org.jaroof.fh8.utils.FhirUtils;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.jaroof.fh8.Constants.*;

@Service
@Slf4j
public class PersonPersistenceService {
    private final FhirContext ctx;
    private final PersonRepository personRepository;

    public PersonPersistenceService(FhirContext ctx, PersonRepository personRepository) {
        this.ctx = ctx;
        this.personRepository =personRepository;
    }

    public void updatePerson(String bundleJson) {
        IParser parser = ctx.newJsonParser();
        Bundle bundle = parser.parseResource(Bundle.class, bundleJson);
        List<Bundle.BundleEntryComponent> entries = bundle.getEntry();
        Person person = null;
        for(Bundle.BundleEntryComponent entry: entries) {
            Resource resource = entry.getResource();
            if (resource != null && "Patient".equals(resource.getResourceType().name())) {
                Patient patient = (Patient)resource;
                String uuid = FhirUtils.getUuidFromIdentifiers(patient.getIdentifier());
                // perform a lookup on the Person in the DB to see if they already exist (UPSERT)
                person = personRepository.findFirstByPatientUuid(uuid);
                if (person == null) {
                    person = new Person();
                    person.setPatientUuid(uuid);
                }
                populatePerson(person, (Patient) resource);

            } else if (resource != null && "Observation".equals(resource.getResourceType().name())) {
                Observation observation = (Observation) resource;
                populateOccupation(person, observation);
            }
        }
        personRepository.save(person);
    }

    /**
     * Populates a Person Entity's occupation and salary with a Patient FHIR resource
     *
     * WARNING: The Person object's state is mutated
     * @param person
     * @param observation
     */
    protected void populateOccupation(Person person, Observation observation) {
        Coding salaryCode = observation.getCode().getCoding().stream()
                .filter(c -> c.getSystem().equals(CODING_SYSTEM_OBSERVATION_VALUE_HL7_V3))
                .filter(c -> c.getCode().equals(HL7_V3_CODE_SALARY))
                .findFirst()
                .orElse(null);
        Coding occupationCode = observation.getCode().getCoding().stream()
                .filter(c -> c.getSystem().equals(CODING_SYSTEM_SNOMED))
                .filter(c -> c.getCode().equals(SNOMED_CODE_OCCUPATION))
                .findFirst()
                .orElse(null);
        if (salaryCode != null && observation.getValueStringType() !=null) {
            String salaryStr = observation.getValueStringType().getValue();
            person.setSalary(Double.parseDouble(salaryStr));
            person.setSalaryUuid(FhirUtils.getUuidFromIdentifiers(observation.getIdentifier()));
        }
        if (occupationCode != null && observation.getValueStringType() !=null) {
            person.setOccupation(observation.getValueStringType().getValueNotNull());
            person.setOccupationUuid(FhirUtils.getUuidFromIdentifiers(observation.getIdentifier()));
        }
    }

    /**
     * Populates a Person Entity with a Patient FHIR resource
     *
     * WARNING: The Person object's state is mutated
     * @param person
     * @param patient
     */
    protected void populatePerson(Person person, Patient patient) {
        person.setFirstName(FhirUtils.getFirstName(patient));
        person.setLastName(FhirUtils.getLastName(patient));
        person.setBirthdate(patient.getBirthDate());
        person.setGender(patient.getGender().toCode());
        //String s = patient.getDeceased().fhirType();
        if (patient.getDeceased().hasType("dateTime") && patient.getDeceasedDateTimeType().getValue() != null) {
            person.setDeceased(true);
        } else {
            person.setDeceased(patient.getDeceasedBooleanType().booleanValue());
        }
        Address address = patient.getAddress().stream().findFirst().orElse(null);
        if (address != null) {
            StringType street1 = address.getLine().stream()
                    .findFirst()
                    .orElse(null);
            if (street1 != null)
                person.setStreet1(street1.getValue());
            person.setCity(address.getCity());
            person.setState(address.getState());
            person.setZipcode(address.getPostalCode());
        }
    }
}
