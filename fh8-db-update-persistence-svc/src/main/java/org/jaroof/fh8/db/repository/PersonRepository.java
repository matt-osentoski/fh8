package org.jaroof.fh8.db.repository;

import org.jaroof.fh8.domain.population.Person;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<Person, Integer> {

    Person findFirstByPatientUuid(String uuid);
}
