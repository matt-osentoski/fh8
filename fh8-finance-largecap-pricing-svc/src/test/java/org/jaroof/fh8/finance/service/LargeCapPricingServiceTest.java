package org.jaroof.fh8.finance.service;

import org.jaroof.fh8.domain.finance.Stock;
import org.jaroof.fh8.finance.domain.LargeCapRange;
import org.jaroof.fh8.random.Dice;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class LargeCapPricingServiceTest {
    
    private LargeCapPricingService largeCapPricingService = null;
    private Stock lastPrice = null;

    @BeforeEach
    void setUp() {
        lastPrice = new Stock();
        lastPrice.setClosingPrice(2123.4321);
        lastPrice.setSymbol("AMD");
        lastPrice.setPriceDate(new Date());
        
        Dice dice = new Dice();
        dice.setSize(20);
        dice.setCriticalHit(20);
        
        LargeCapRange largeCapRange = new LargeCapRange();
        largeCapRange.setNewPriceLow(2000);
        largeCapRange.setNewPriceHigh(6000);
        
        largeCapPricingService = new LargeCapPricingService(lastPrice, largeCapRange, dice);
    }

    @Test
    void generateRandomPrice() {
        Stock stock = largeCapPricingService.generateRandomPrice(new Date().getTime());
        double closingPrice = stock.getClosingPrice();
        assertNotEquals(0, closingPrice);
    }

    @Test
    void randomClosingPriceAlgo() {
        double v = largeCapPricingService.randomClosingPriceAlgo(lastPrice.getClosingPrice());
        assertNotEquals(lastPrice.getClosingPrice(), v);
        assertNotEquals(0, v);
    }
}