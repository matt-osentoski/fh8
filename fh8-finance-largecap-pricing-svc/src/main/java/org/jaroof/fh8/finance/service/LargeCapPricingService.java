package org.jaroof.fh8.finance.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.ParetoDistribution;
import org.jaroof.fh8.domain.finance.Stock;
import org.jaroof.fh8.finance.Constants;
import org.jaroof.fh8.finance.domain.LargeCapRange;
import org.jaroof.fh8.random.Dice;
import org.jaroof.fh8.utils.NumberUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;
import java.util.Random;

/**
 * This service manages the pricing of a Large Cap index fund (stock)
 */
@Service
@Slf4j
public class LargeCapPricingService {

    private Stock lastStockPrice;
    private LargeCapRange largeCapRange;
    private Dice dice;

    public LargeCapPricingService(Stock lastStockPrice, LargeCapRange largeCapRange, Dice dice) {
        this.lastStockPrice = lastStockPrice;
        this.largeCapRange = largeCapRange;
        this.dice = dice;
    }

    /**
     * Generates a new random price for a specific day (the epoch value).
     * @param epoch The epoch value that corresponds to a day
     * @return A new Stock object containing a randomly generated closing price, specifically
     */
    public Stock generatePrice(long epoch) {
        Stock stock = null;
        if (lastStockPrice == null || lastStockPrice.getClosingPrice() == 0) {
            stock = this.generateFirstStock(epoch);
        } else {
            stock = this.generateRandomPrice(epoch);
        }
        lastStockPrice = stock;
        return stock;
    }

    /**
     * Randomly generates the closing price of a stock based off the last price and custom algorithms using different
     * distributions to vary the thickness of the tails.
     *
     * @param epoch
     */
    protected Stock generateRandomPrice(long epoch) {
        Stock stock = new Stock();
        stock.setSymbol(Constants.LARGE_CAP_INDEX_SYMBOL);
        stock.setPriceDate(new Date(epoch));
        double randomCLosingPrice = this.randomClosingPriceAlgo(lastStockPrice.getClosingPrice());
        double roundedPrice = NumberUtils.roundDouble(randomCLosingPrice,4);
        stock.setClosingPrice(roundedPrice);
        // TODO add logic to calculate random opening/high/low prices
        // Maybe put this in a shared core class (Could be the same as used in the generateNewPrice() method below)
        return stock;
    }

    /**
     * This method contains the algorithm used for pricing a stock (index in this case). The algo is dependent on
     * the last closing price to base the current price from.
     * @param closingPrice The previous closing price.
     * @return A new closing price that is randomly generated
     */
    protected double randomClosingPriceAlgo(double closingPrice) {
        double newPrice = closingPrice;
        boolean criticalHit = false;
        Random rand = new Random();
        double percent = 0;
        int rolledDie = rand.nextInt(dice.getSize()) + 1;
        // If a critical hit is rolled, use a more extreme distribution (Pareto, Cauchy, etc.)
        if (rolledDie == dice.getCriticalHit()) {
            ParetoDistribution p = new ParetoDistribution(1,2.25);
            percent = p.sample() - 1; // subtract one to make the starting range closer to zero.

            // Pareto doesn't have positive / negative values like a normal distribution
            // so, randomly make the percent negative
            if (rand.nextBoolean()) {
                percent = percent * -1;
            }
            log.info("Critical Hit percent: " + percent);
        } else {
            NormalDistribution n = new NormalDistribution();
            percent = n.sample();
        }

        newPrice = closingPrice + (closingPrice * (percent/100));
        return newPrice;
    }

    /**
     * This method is called if the Kafka topic was empty.  This is the starting Stock object which will
     * seed this index and the Kafka topic.  Since pricing is dependent on the last closing price, this first
     * Stock has to have a price range that would make sense for this index. Additional prices will then fluctuate
     * randomly
     *
     * @param epoch
     * @return
     */
    protected Stock generateFirstStock(long epoch) {
        Stock stock = new Stock();
        stock.setSymbol(Constants.LARGE_CAP_INDEX_SYMBOL);
        stock.setPriceDate(new Date(epoch));
        Random rand = new Random();
        double randomCLosingPrice =
                largeCapRange.getNewPriceLow() + (largeCapRange.getNewPriceHigh() - largeCapRange.getNewPriceLow()) *
                        rand.nextDouble();
        double roundedPrice = NumberUtils.roundDouble(randomCLosingPrice,4);
        stock.setClosingPrice(roundedPrice);
        // TODO add logic to calculate random opening/high/low prices
        // Maybe put this in a shared core class
        return stock;
    }

    /**
     * Set the lastStockPrice bean (singleton) based on the JSON representation coming from the Kafka topic.
     * @param jsonStock
     */
    public void setLastPriceFromJson(String jsonStock) {
        Stock stock = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            stock = objectMapper.readValue(jsonStock, Stock.class);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        lastStockPrice = stock;
    }
}
