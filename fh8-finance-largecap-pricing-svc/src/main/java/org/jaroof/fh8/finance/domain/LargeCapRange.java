package org.jaroof.fh8.finance.domain;

import lombok.Data;

@Data
public class LargeCapRange {
    private long newPriceLow;
    private long newPriceHigh;
}
