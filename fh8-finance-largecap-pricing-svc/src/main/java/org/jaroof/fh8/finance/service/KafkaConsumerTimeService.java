package org.jaroof.fh8.finance.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.jaroof.fh8.domain.finance.Stock;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.KafkaListenerConfigUtils;
import org.springframework.kafka.config.KafkaListenerEndpointRegistry;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class KafkaConsumerTimeService {

    private KafkaListenerEndpointRegistry registry; // Used to lookup this listener for manual starting/stopping
    private LargeCapPricingService largeCapPricingService;
    private KafkaProducerPricingService kafkaProducerPricingService;

    public KafkaConsumerTimeService(@Qualifier(KafkaListenerConfigUtils.KAFKA_LISTENER_ENDPOINT_REGISTRY_BEAN_NAME)
            KafkaListenerEndpointRegistry registry, LargeCapPricingService largeCapPricingService,
                                    KafkaProducerPricingService kafkaProducerPricingService) {
        this.registry = registry;
        this.largeCapPricingService = largeCapPricingService;
        this.kafkaProducerPricingService = kafkaProducerPricingService;
    }

    @KafkaListener(topics = "#{'${kafka.topic.time.epoch}'.split(',')}",
            id = "timeListener",
            autoStartup = "false",
            groupId = "${spring.kafka.consumer.group-id}")
    public void consume(String message) {
        log.info(String.format("$$ -> Consumed Message -> %s",message));
        long epoch = Long.parseLong(message);
        Stock stock = largeCapPricingService.generatePrice(epoch);
        // Send the message as JSON to a Kafka Producer
        ObjectMapper objectMapper = new ObjectMapper();
        String stockJson = null;
        try {
            stockJson = objectMapper.writeValueAsString(stock);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        kafkaProducerPricingService.produceAsyncMessage(stockJson);
    }

    /**
     * Delay the start of the time Listener to give the largeCap pricing Listener time to grab the last closing price.
     *
     * (NOTE: We want this scheduled job to run only once.  We're using the Dead man's solution. (fixedDelay=Long.MAX_VALUE)
     * in other words, you'll be dead before it runs again.  Hat tip to this StackOverflow answer. I <3 this:
     * https://stackoverflow.com/a/55818587)
     */
    @Scheduled(initialDelayString = "${time.loop.fixed.delay}", fixedDelay=Long.MAX_VALUE)
    public void delayTimeLoopStart() {
        log.info("Starting the KafkaConsumerTimeService...");
        registry.getListenerContainer("timeListener").start();
    }

}
