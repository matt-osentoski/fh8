package org.jaroof.fh8.finance;

import org.apache.kafka.clients.admin.NewTopic;
import org.jaroof.fh8.domain.finance.Stock;
import org.jaroof.fh8.finance.domain.LargeCapRange;
import org.jaroof.fh8.random.Dice;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class ApplicationConfig {
    @Value("${kafka.topic.largecap.prices}")
    private String largeCapTopic;

    @Value("${largecap.new.price.low}")
    private String newPriceLowStr;

    @Value("${largecap.new.price.high}")
    private String newPriceHighStr;

    @Value("${dice.size}")
    private String diceSizeStr;

    @Value("${dice.critical.hit}")
    private String diceCriticalHitStr;

    @Bean
    public Stock lastStockPrice() {
        return new Stock();
    }

    @Bean
    public Dice dice() {
        Dice dice = new Dice();
        dice.setSize(Integer.parseInt(diceSizeStr));
        dice.setCriticalHit(Integer.parseInt(diceCriticalHitStr));
        return dice;
    }

    @Bean
    public LargeCapRange largeCapRange() {
        LargeCapRange largeCapRange = new LargeCapRange();
        largeCapRange.setNewPriceLow(Long.parseLong(newPriceLowStr));
        largeCapRange.setNewPriceHigh(Long.parseLong(newPriceHighStr));
        return largeCapRange;
    }

    /**
     * Creates a new topic
     * @return
     */
    @Bean
    public NewTopic largeCapTopic() {
        return TopicBuilder.name(largeCapTopic)
                .partitions(1)
                .replicas(3)
                // Never delete any messages in this topic
                .config("retention.ms", "-1")
                .config("retention.bytes", "-1")
                .build();
    }
}
