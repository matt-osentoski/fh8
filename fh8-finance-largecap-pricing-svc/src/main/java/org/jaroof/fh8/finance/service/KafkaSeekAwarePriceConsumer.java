package org.jaroof.fh8.finance.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.TopicPartition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.KafkaListenerConfigUtils;
import org.springframework.kafka.config.KafkaListenerEndpointRegistry;
import org.springframework.kafka.listener.AbstractConsumerSeekAware;
import org.springframework.kafka.listener.MessageListenerContainer;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Map;

/**
 * To make sure we get the timing right, When the application starts up, it should grab the latest price. If the price
 * is null, then a beginning starting price should be created
 */
@Slf4j
@Service
public class KafkaSeekAwarePriceConsumer extends AbstractConsumerSeekAware {

    private KafkaListenerEndpointRegistry registry;
    private LargeCapPricingService largeCapPricingService;
    private String largeCapTopic;

    public KafkaSeekAwarePriceConsumer(@Qualifier(KafkaListenerConfigUtils.KAFKA_LISTENER_ENDPOINT_REGISTRY_BEAN_NAME)
                                               KafkaListenerEndpointRegistry registry,
                                       @Value("${kafka.topic.largecap.prices}") String largeCapTopic,
                                       LargeCapPricingService largeCapPricingService) {
        this.registry = registry;
        this.largeCapTopic = largeCapTopic;
        this.largeCapPricingService = largeCapPricingService;
    }

    /**
     * On startup (PostConstruct) seek to the end of the topic and set the price.
     *
     * (NOTE: This wasn't working for starting the Listener. I'm leaving it here for documentation purposes)
     */
    @PostConstruct
    public void init() {
        // NOTE: Putting beans and startup params here wasn't working, see the 'onApplicationEvent()' method for
        // details.
    }

    /**
     * (IMPORTANT!!!!: This step really isn't necessary.  We could have the Listener startup automatically using
     * autoStartup = "true" (The default setting), but this illustrates how to go about starting the listener after all the Spring
     * beans and context have loaded. )
     *
     * Runs after the entire AppContext has been loaded and is ready
     * NOTE: I was getting null pointer exceptions for the registry bean in the 'init()' method above
     * using the @PostConstruct annotation.  This is a better place to put these startup params.
     * @param event
     */
    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) {
        // Use the 'id' of the Listener to find the appropriate ListenerContainer
        log.info(String.format("Starting the KafkaSeekAwarePriceConsumer..."));
        registry.getListenerContainer("priceSeekListener").start();
    }

    /**
     * Override this method to change the initial seek location for the topic.  In this case,
     * I want to start at the last message which is -1 (relative to the end)
     * @param assignments
     * @param callback
     */
    @Override
    public void onPartitionsAssigned(Map<TopicPartition, Long> assignments, ConsumerSeekCallback callback) {
        // Go to the last message (-1 from the end)
        callback.seekRelative(largeCapTopic, 0, -1, false);
    }

    // NOTE: We're using a UUID to ensure that this is a unique group to grab the latest price
    @KafkaListener(topics = "#{'${kafka.topic.largecap.prices}'.split(',')}",
            id = "priceSeekListener",
            autoStartup = "false",  // Listener starts manually using the registry in the onApplicationEvent() method above
            groupId = "lrgcap-price-#{ T(java.util.UUID).randomUUID().toString() }")
    public void consume(String message) {
        log.info(String.format("$$ -> Consumed Message -> %s",message));
        //Parse the last pricing message and set it to the latest price, via a bean
        largeCapPricingService.setLastPriceFromJson(message);

        // After we retrieve the latest price, shut off this listener
        log.info(String.format("Shutting down the KafkaSeekAwarePriceConsumer..."));
        registry.getListenerContainer("priceSeekListener").stop();
    }
}
