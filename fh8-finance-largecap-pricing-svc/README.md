# fh8-finance-largecap-pricing-svc
This module is a Microservice that consumes time messages from a kafka topic. For this application we assume each tick is a virtual day.
Create a random price for a fictional Large Cap index fund. (roughly based off the S&P 500)

## Application Flow
- Consume time messages from a Kafka topic. Each message is a time unit in the future (tick)
- Randomly calculate the financial instrument price. If this is the first price, use a sensible starting point (Ex: 2000 - 4000)
- Convert the price object into JSON
- Using a Kafka producer, place the person JSON objects onto a topic.


## Manually build the module
This module is normally built from the parent POM, but can be built individually.
```
mvn clean install
```

