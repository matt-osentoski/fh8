package org.jaroof.fh8.domain.population;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="person")
@Data
public class Person {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "patient_uuid")
    private String patientUuid;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "gender")
    private String gender;

    @Column(name = "deceased", columnDefinition = "TINYINT(1)", nullable = false)
    private boolean deceased;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @Column(name = "birthdate")
    private Date birthdate;

    @Column(name = "street1")
    private String street1;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "zipcode")
    private String zipcode;

    @Column(name = "occupation")
    private String occupation;

    @Column(name = "occupation_uuid")
    private String occupationUuid;

    @Column(name = "salary")
    private double salary;

    @Column(name = "salary_uuid")
    private String salaryUuid;

    /**
     * When this population object is being carried through the streaming framework, this field contains
     * the virtual timeline.
     */
    @Transient
    private Date virtualDate;
}
