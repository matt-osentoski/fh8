package org.jaroof.fh8.domain.lookup;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name="occupational_wages")
@Data
public class OccupationalWage {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "occupational_title")
    private String title;

    @Column(name = "wage")
    private double wage;
}
