package org.jaroof.fh8.domain.lookup;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name="actor_names")
@Data
public class PersonName {

    public static final int MAX_AGE = 125;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "birth_year")
    private String birthYear;

    @Column(name = "death_year")
    private String deathYear;

    @Column(name = "gender")
    private String gender;
}
