package org.jaroof.fh8.domain.finance;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name="stock")
public class Stock {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "symbol")
    private String symbol;

    @Column(name = "volume")
    private int volume;

    @Column(name = "opening_price")
    private double openingPrice;

    @Column(name = "closing_price")
    private double closingPrice;

    @Column(name = "low_price")
    private double lowPrice;

    @Column(name = "high_price")
    private double highPrice;

    @Column(name = "price_date")
    private Date priceDate;

    private String period;
}
