package org.jaroof.fh8.domain.finance;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@Table(name="treasuries")
public class Treasury {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private String id;

    @Column(name = "price_date")
    private Date priceDate;

    @Column(name = "1_year")
    private double c1year;

    @Column(name = "2_year")
    private double c2year;

    @Column(name = "3_year")
    private double c3year;

    @Column(name = "5_year")
    private double c5year;

    @Column(name = "7_year")
    private double c7year;

    @Column(name = "10_year")
    private double c10year;

    @Column(name = "20_year")
    private double c20year;

    @Column(name = "30_year")
    private double c30year;
}