package org.jaroof.fh8.domain.lookup;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name="common_codes")
@Data
public class CommonCode {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "icd_10")
    private String icd10;

    @Column(name = "snomed")
    private String snomed;

    @Column(name = "diagnosis")
    private String diagnosis;

}
