package org.jaroof.fh8.domain.lookup;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="age_distribution")
public class AgeDistribution {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "age")
    private int age;
}
