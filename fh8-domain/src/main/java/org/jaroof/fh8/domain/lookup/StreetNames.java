package org.jaroof.fh8.domain.lookup;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name="street_names")
@Data
public class StreetNames {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "full_street_name")
    private String fullStreetName;

    @Column(name = "street_name")
    private String streetName;

    @Column(name = "street_type")
    private String streetType;

    @Column(name = "post_direction")
    private String postDirection;
}
