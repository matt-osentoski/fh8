package org.jaroof.fh8.domain.lookup;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="life_expectancy")
public class LifeExpectancy {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "age")
    private int age;

    @Column(name = "male_years")
    private double maleYears;

    @Column(name = "female_years")
    private double femaleYears;
}
