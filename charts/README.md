# Helm Charts for fh8
This directory contains Helm charts for the fh8 application stack (Java applications)

>(NOTE: fh8 Applications that are not written using Java may live in a different repository)

## Deploying the charts
To deploy a chart, run the following commands. We'll use 'fh8-randomizer-ws' as an example:

```
cd <fh8_HOME>/charts
helm upgrade --install --namespace fh8 --values fh8-randomizer-ws/values.yaml fh8-randomizer-ws fh8-randomizer-ws/
```

## Uninstall a helm application
To uninstall an helm application, run the following command. Again, using 'fh8-randomizer-ws' as an example:

```
helm uninstall --namespace fh8 fh8-randomizer-ws
```

## Helmfile for installing all apps
Helmfile (Not to be confused with Helm) is an application that can install multiple Helm applications with
a single command.  Look in the `helmfile` folder at this location for more information.



