package org.jaroof.fh8.time.service;

import lombok.extern.slf4j.Slf4j;
import org.jaroof.fh8.time.domain.TimeKeeper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class KafkaTimeService {

    private KafkaTemplate<String, String> kafkaTemplate;
    private String topic;
    private TimeKeeper timeKeeper;

    public KafkaTimeService(KafkaTemplate<String, String> kafkaTemplate, TimeKeeper timeKeeper,
                            @Value("${kafka.topic.time.epoch}") String topic) {
        this.kafkaTemplate = kafkaTemplate;
        this.timeKeeper = timeKeeper;
        this.topic = topic;
    }

    @KafkaListener(topics = "#{'${kafka.topic.time.epoch}'.split(',')}")
    public void consume(String message) {
        this.updateCurrentTimeFromMessage(message);
    }

    public void sendMessage(String message) {
        log.debug(String.format("Producing message -> %s", message));
        this.kafkaTemplate.send(topic, message);
        this.updateCurrentTimeFromMessage(message);
    }

    protected void updateCurrentTimeFromMessage(String message) {
        long currentTime = Long.parseLong(message);
        this.timeKeeper.setCurrentTime(currentTime);
    }
}
