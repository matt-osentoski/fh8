package org.jaroof.fh8.time.domain;

import lombok.Data;

@Data
public class TimeKeeper {

    private Long currentTime;
}
