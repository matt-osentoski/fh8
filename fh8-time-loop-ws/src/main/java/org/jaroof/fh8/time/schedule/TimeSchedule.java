package org.jaroof.fh8.time.schedule;

import lombok.extern.slf4j.Slf4j;
import org.jaroof.fh8.dto.ApplicationState;
import org.jaroof.fh8.time.domain.TimeKeeper;
import org.jaroof.fh8.time.domain.TimeParams;
import org.jaroof.fh8.time.service.KafkaTimeService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@Component
public class TimeSchedule {

    private TimeKeeper timeKeeper;
    private TimeParams timeParams;
    private KafkaTimeService kafkaTimeService;
    private ApplicationState applicationState;

    public TimeSchedule(TimeKeeper timeKeeper, TimeParams timeParams, KafkaTimeService kafkaTimeService,
                        ApplicationState applicationState) {
        this.timeKeeper = timeKeeper;
        this.timeParams = timeParams;
        this.kafkaTimeService = kafkaTimeService;
        this.applicationState = applicationState;
    }

    /**
     * Wait a few seconds to let Kafka catch up, before beginning
     */
    @Scheduled(fixedDelayString = "${timeline.fixed.delay}", initialDelayString = "${timeline.initial.delay}")
    public void timeLoop() {
        if (this.applicationState.isActive()) {
            long epoch = 0;
            if (this.timeKeeper.getCurrentTime() == null) {
                Date now = new Date();
                epoch =now.getTime();
            } else {
                epoch = this.timeKeeper.getCurrentTime();
            }

            Long updatedTime = incrementTime(epoch);
            log.info("Sending new time: " + updatedTime.toString());
            kafkaTimeService.sendMessage(updatedTime.toString());
        }
    }

    protected Long incrementTime(long epoch) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(epoch));
        cal.add(Calendar.HOUR_OF_DAY, timeParams.getHoursAhead());
        return cal.getTime().getTime();
    }
}
