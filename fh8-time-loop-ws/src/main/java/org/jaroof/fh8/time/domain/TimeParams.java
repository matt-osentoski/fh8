package org.jaroof.fh8.time.domain;

import lombok.Data;

@Data
public class TimeParams {
    /**
     * Realtime delay between time changes (milliseconds).  Considered one 'tick'
     */
    private long fixedDelay;

    /**
     * The number of hours to move the clock up for each fixedDelay tick
     */
    private int hoursAhead;

    /**
     * Initially delay the scheduler by 10 seconds to give the Kafka consumer a chance to catch up
     */
    private long initialDelay;
}
