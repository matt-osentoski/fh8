package org.jaroof.fh8.time.controller;

import lombok.extern.slf4j.Slf4j;
import org.jaroof.fh8.dto.ApplicationState;
import org.jaroof.fh8.time.Application;
import org.jaroof.fh8.time.domain.TimeKeeper;
import org.jaroof.fh8.time.domain.TimeParams;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("v1/timeline")
@Slf4j
public class TimeController {

    private ApplicationState applicationState;
    private TimeParams timeParams;
    private TimeKeeper timeKeeper;

    public TimeController(ApplicationState applicationState, TimeParams timeParams, TimeKeeper timeKeeper) {
        this.applicationState = applicationState;
        this.timeParams = timeParams;
        this.timeKeeper = timeKeeper;
    }

    @RequestMapping(value = "/state", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> state() {
        ApplicationState tmpState = null;
        // Check that a time has been registered before showing the service as ready.
        if (this.applicationState.isActive() && this.timeKeeper.getCurrentTime() != null &&
                this.timeKeeper.getCurrentTime().longValue() > 0 ) {
            tmpState = this.applicationState;
        } else {
            tmpState = new ApplicationState();
            tmpState.setActive(false);
        }
        return new ResponseEntity<>(tmpState, HttpStatus.OK);
    }

    @RequestMapping(value = "/state", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> state(@RequestBody ApplicationState params) {
        this.applicationState.setActive(params.isActive());
        log.info("Application active state is now: " + this.applicationState.isActive());
        return new ResponseEntity<>(this.applicationState, HttpStatus.OK);
    }

    @RequestMapping(value = "/params", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> params() {
        return new ResponseEntity<>(this.timeParams, HttpStatus.OK);
    }
}
