package org.jaroof.fh8.time;

import org.apache.kafka.clients.admin.NewTopic;
import org.jaroof.fh8.dto.ApplicationState;
import org.jaroof.fh8.time.domain.TimeKeeper;
import org.jaroof.fh8.time.domain.TimeParams;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class ApplicationConfig {

    @Value("${timeline.fixed.delay}")
    private String fixedDelay;

    @Value("${timeline.hours.ahead}")
    private String hoursAhead;

    @Value("${timeline.initial.delay}")
    private String initialDelay;

    @Value("${kafka.topic.time.epoch}")
    private String timeEpochTopicName;

    @Bean
    public Docket projectApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.regex("/v1.*"))
                .build();
    }

    @Bean
    public TimeKeeper latestTime() {
        return new TimeKeeper();
    }

    @Bean
    public ApplicationState applicationState() {
        return new ApplicationState();
    }

    @Bean
    public TimeParams timeParams() {
        TimeParams params = new TimeParams();
        params.setFixedDelay(Long.parseLong(fixedDelay)); // every 2 mins
        params.setHoursAhead(Integer.parseInt(hoursAhead)); // 24 hours
        params.setInitialDelay(Long.parseLong(initialDelay));
        return params;
    }

    /**
     * Creates a new topic
     * @return
     */
    @Bean
    public NewTopic timeEpochTopic() {
        return TopicBuilder.name(timeEpochTopicName)
                .partitions(1)
                .replicas(3)
                .build();
    }
}
