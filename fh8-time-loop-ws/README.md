# fh8-time-loop-ws
This module is a Web Service that manages the timelines for the fh8 applications
and processes.  The service starts in a 'paused' state.  You will have to change
the state using a RESTful endpoint, listed below.  Once started, the service will loop
and send an updated time to a Kafka topic. The time is in an Epoch format, for portability.

## Application Flow
- The time loop service starts with an active state set to 'false'
- Make a POST to the '/v1/timeline/state' endpoint setting the active flag to 'true'
- A loop sends a timing event to a Kakfa topic, containing the next time increment

## Swagger endpoints
This web service uses Swagger for contract generation of the Web Service endpoints. By default
swagger can be reached at the following URL:

[http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)

## Manually build the module
This module is normally build from the parent POM, but can be built individually.
```
mvn clean install
```

