package org.jaroof.fh8.fhir.service;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import lombok.extern.slf4j.Slf4j;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Resource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class FhirServerService {

    private final FhirContext ctx;
    private final String fhirBaseUrl;

    public FhirServerService(FhirContext ctx, @Value("${fhir.server.base.url}") String fhirBaseUrl) {
        this.ctx = ctx;
        this.fhirBaseUrl = fhirBaseUrl;
    }

    public void insertFhirBundle(String bundleJson) {
        IParser parser = ctx.newJsonParser();
        Bundle bundle = parser.parseResource(Bundle.class, bundleJson);
        IGenericClient client = ctx.newRestfulGenericClient(fhirBaseUrl);

        if (bundle.getType() == Bundle.BundleType.TRANSACTION) {
            log.info("Executing Bundle transaction");
            Bundle execute = client.transaction().withBundle(bundle).encodedJson().execute();
            log.info("Executed Bundle: " + execute.toString());
        }
    }
}
