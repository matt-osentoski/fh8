# fh8-fhir-insert-persistence-svc
This module is a Microservice that consumes FHIR Patient (and related) resources from a Kafka topic then
inserts them into a FHIR server.  All FHIR resources are assumed to be packaged in a bundle
during ingestion.

>(NOTE: This service performs bulk insert operations into a FHIR server. This service
>is not meant to be idempotent.  For updates/idempotent operations a different service should be used)

## Application Flow
- Consume a FHIR Bundle from a Kafka topic.
- Extract individual FHIR resources from the bundle
- POST the resources to a compliant FHIR server to act as the 'source of truth' for this
data.

## Manually build the module
This module is normally build from the parent POM, but can be built individually.
```
mvn clean install
```

