# fh8-population-ws
This module is a Web Service that randomly generates individuals and populations
as FHIR objects.  The FHIR objects are then Produced into a topic for consumption.

Individuals are created using lookup tables from a relational database and random values.

## Application Flow
- A Restful POST is made, defining the population size and locality
- Individuals are randomly created along with professions, etc. 
- Individuals and supporting objects are converted into FHIR resources and packaged into a `Bundle`
- FHIR Bundles are produced into a Kafka topic for later consumption.

## Manually build the module
This module is normally build from the parent POM, but can be built individually.
```
mvn clean install
```

