package org.jaroof.fh8.randomzier.repository;

import org.jaroof.fh8.domain.lookup.StreetNames;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface StreetNamesRepository extends CrudRepository<StreetNames, Integer> {
    @Query(value="SELECT * FROM street_names ORDER BY RAND() LIMIT :count", nativeQuery = true)
    List<StreetNames> findRandomNativeMysql(@Param("count")int count);
}
