package org.jaroof.fh8.randomzier.repository;

import org.jaroof.fh8.domain.lookup.CommonCode;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CommonCodesRepository extends CrudRepository<CommonCode, Integer> {

    @Query(value="SELECT * FROM common_codes ORDER BY RAND() LIMIT :count", nativeQuery = true)
    List<CommonCode> findRandomNativeMysql(@Param("count")int count);
}
