package org.jaroof.fh8.randomzier.service;

import ca.uhn.fhir.context.FhirContext;
import org.hl7.fhir.r4.model.*;
import org.jaroof.fh8.domain.lookup.PersonName;
import org.jaroof.fh8.domain.lookup.StreetNames;
import org.jaroof.fh8.domain.lookup.Zipcode;
import org.jaroof.fh8.utils.DateUtils;
import org.jaroof.fh8.utils.FhirUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

@Service
public class RandomPatientService {

    private FhirContext fhirContext;
    private EmploymentObservationService employmentObservationService;
    private RandomLookupCachingService randomLookupCachingService;
    private List<Integer> ageDistributionList;

    public RandomPatientService(FhirContext fhirContext, EmploymentObservationService employmentObservationService,
                                RandomLookupCachingService randomLookupCachingService,
                                List<Integer> ageDistributionList) {
        this.fhirContext = fhirContext;
        this.employmentObservationService = employmentObservationService;
        this.randomLookupCachingService = randomLookupCachingService;
        this.ageDistributionList = ageDistributionList;
    }

    public String createPatientBundleJson(Integer zipcode, int radius) {
        Bundle bundle = createPatientBundle(zipcode, radius);
        String fhirJson = fhirContext.newJsonParser().setPrettyPrint(false).encodeResourceToString(bundle);
        return  fhirJson;
    }

    public Bundle createPatientBundle(Integer zipcode, int radius) {
        Patient patient = this.patientBuilder(zipcode, radius);
        Bundle bundle = new Bundle();
        bundle.setType(Bundle.BundleType.TRANSACTION);

        Bundle.BundleEntryComponent entry = bundle.addEntry();
        //entry.setFullUrl(FULL_URL_SYSTEM + UUID.randomUUID().toString());
        entry.setResource(patient);
        entry.getRequest()
                .setUrl("Patient")
                .setMethod(Bundle.HTTPVerb.POST);

        // Add Occupational resources for this patient
        List<Observation> observations = new ArrayList<>();
        if (DateUtils.studentDate().before(patient.getBirthDate())) {
            observations = employmentObservationService.getStudentEmploymentObservations(patient);
        } else if (DateUtils.seniorDate().after(patient.getBirthDate())) {
            observations = employmentObservationService.getRetiredEmploymentObservations(patient);
        } else {
            observations = employmentObservationService.getRandomEmploymentObservations(patient);
        }
        for (Observation observation: observations) {
            Bundle.BundleEntryComponent observationEntry = bundle.addEntry();
            //observationEntry.setFullUrl(FULL_URL_SYSTEM + UUID.randomUUID().toString());
            observationEntry.setResource(observation);

            observationEntry.getRequest()
                    .setUrl("Observation")
                    .setMethod(Bundle.HTTPVerb.POST);
        }
        return bundle;
    }

    protected Patient patientBuilder(Integer zipcode, int radius) {
        Zipcode randomZipcode = randomLookupCachingService.getRandomZipcode(zipcode.intValue(), radius);
        StreetNames randomStreetName = randomLookupCachingService.getRandomStreetName();
        PersonName randomPersonName = randomLookupCachingService.getRandomPerson();

        Patient patient = new Patient();
        patient.setId(UUID.randomUUID().toString());
        patient.setActive(true);
        BooleanType notDeceased = new BooleanType(false);
        patient.setGender(this.getGender(randomPersonName));
        patient.setDeceased(notDeceased);
        patient.setIdentifier(FhirUtils.createSingleUuidIdentifier());
        patient.setMeta(FhirUtils.createBaseMeta());
        List<HumanName> humanNames = getHumanNames(randomPersonName);
        patient.setName(humanNames);
        patient.setAddress(getAddresses(randomStreetName, randomZipcode));
        patient.setBirthDate(getRandomBirthdate());
        return patient;
    }

    protected List<Address> getAddresses(StreetNames randomStreetNames, Zipcode zipcode) {
        List<Address> addresses = new ArrayList<>();
        Address address = new Address();
        address.setCity(zipcode.getCity());
        address.setState(zipcode.getState());
        address.setUse(Address.AddressUse.HOME);
        address.setCountry("US");
        address.setType(Address.AddressType.BOTH);
        address.setLine(getRandomAddressLines(randomStreetNames));
        address.setPostalCode(zipcode.getZipcode().toString());
        addresses.add(address);
        return  addresses;
    }

    protected List<StringType> getRandomAddressLines(StreetNames randomStreetNames) {
        List<StringType> lines = new ArrayList<>();
        int addressNumber = getRandomAddressNumber();
        String addressNumberStr = Integer.toString(addressNumber);
        String fullAddress = addressNumberStr + " " + randomStreetNames.getFullStreetName();
        lines.add(new StringType(fullAddress));
        return lines;
    }

    protected int getRandomAddressNumber() {
        Random rand = new Random();
        int i = rand.nextInt(99999 - 1) + 1;
        return i;
    }

    protected List<HumanName> getHumanNames(PersonName randomPersonName) {
        List<HumanName> humanNames = new ArrayList<>();
        HumanName humanName = new HumanName();
        String name = randomPersonName.getName();
        humanName.setText(name); // Full name
        NamePairs namePairs = splitNameElements(name);
        humanName.setFamily(namePairs.family);
        humanName.setGiven(namePairs.given);
        humanNames.add(humanName);
        return humanNames;
    }

    protected NamePairs splitNameElements(String name) {
        NamePairs namePairs = new NamePairs();

        String[] nameParts = name.split(" ");
        List<StringType> givenNames = new ArrayList<>();
        if (nameParts.length == 1) {
            givenNames.add(new StringType(name));
            namePairs.given = givenNames;
        } else if (nameParts.length == 2) {
            givenNames.add(new StringType(nameParts[0]));
            namePairs.given = givenNames;
            namePairs.family = nameParts[1];
        } else if (nameParts.length > 2) {
            for(int x=0; x<nameParts.length-1; x++) {
                givenNames.add(new StringType(nameParts[x]));
            }
            namePairs.given = givenNames;
            namePairs.family = nameParts[nameParts.length-1];
        }
        return namePairs;
    }

    protected static class NamePairs {
        protected String family;
        protected List<StringType> given = new ArrayList<>();
    }

    protected Date getRandomBirthdate() {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int generatedAge = ageDistributionList.get(new Random().nextInt(ageDistributionList.size()));
        int birthYear = currentYear - generatedAge;

        int month = 12;
        int dayOfMonth = 31;
        if (birthYear == currentYear) {
            month = Calendar.getInstance().get(Calendar.MONTH)+1;
            dayOfMonth = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        }
        Random random = new Random();
        int minDay = (int) LocalDate.of(birthYear, 1, 1).toEpochDay();
        int maxDay = (int) LocalDate.of(birthYear, 12, 31).toEpochDay();
        long randomDay = minDay + random.nextInt(maxDay - minDay);

        LocalDate randomBirthDate = LocalDate.ofEpochDay(randomDay);

        // Yow... rough conversion..
        Date birthDate = Date.from(randomBirthDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        return birthDate;
    }

    protected Enumerations.AdministrativeGender getGender(PersonName personName) {
        Enumerations.AdministrativeGender gender = null;
        if (personName.getGender() != null && "Male".equals(personName.getGender())) {
            gender = Enumerations.AdministrativeGender.MALE;
        } else if (personName.getGender() != null && "Female".equals(personName.getGender())) {
            gender = Enumerations.AdministrativeGender.FEMALE;
        } else {
            gender = Enumerations.AdministrativeGender.UNKNOWN;
        }
        return gender;
    }
}
