package org.jaroof.fh8.randomzier.controller;

import org.jaroof.fh8.dto.BulkLoadParams;
import org.jaroof.fh8.randomzier.service.PatientControllerService;
import org.jaroof.fh8.randomzier.service.RandomPatientService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.HttpStatus.OK;

@RestController()
@RequestMapping("v1/fhir/patient")
public class PatientController {


    private RandomPatientService randomPatientService;
    private PatientControllerService patientControllerService;

    public PatientController(RandomPatientService randomPatientService, PatientControllerService patientControllerService) {
        this.randomPatientService = randomPatientService;
        this.patientControllerService = patientControllerService;
    }

    /**
     * Displays what a random patient will look like as a JSON FHIR object.
     * @return JSON FHIR Object
     * @throws Exception
     */
    @RequestMapping(value = "/random", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getRandom() throws Exception {
        String fhirJson = randomPatientService.createPatientBundleJson(48081, 10);
        return new ResponseEntity<>(fhirJson, OK);
    }

    @RequestMapping(value = "/seed-population", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, String>> seedPopulation(@RequestBody BulkLoadParams params) {
        patientControllerService.createPatientsAndProduceFhir(params);
        Map<String, String> response = new HashMap<>();
        response.put("message", "Request is processing");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
