package org.jaroof.fh8.randomzier.service;

import org.jaroof.fh8.dto.BulkLoadParams;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.concurrent.CompletableFuture;

/**
 * This class is a service wrapper for the PatientController to add long running async methods
 */
@Service
public class PatientControllerService {

    private RandomPatientService randomPatientService;
    private KafkaPersonService kafkaPersonService;

    public PatientControllerService(RandomPatientService randomPatientService, KafkaPersonService kafkaPersonService) {
        this.randomPatientService = randomPatientService;
        this.kafkaPersonService = kafkaPersonService;
    }

    /**
     * This is a long running method that creates FHIR objects as Bundle resources then hands
     * them off to Kafka (producer)
     * @param params RESTful Parameters
     */
    @Async
    public void createPatientsAndProduceFhir(@RequestBody BulkLoadParams params) {
        CompletableFuture<String> completableFuture = new CompletableFuture<>();
        for (int c=0; c<params.getCount(); c++) {
            /*
             * NOTE: To simplify the Patient processing with supporting Resources (Observations,etc.) Bundles should
             * Only contain resources about that particular patient.  Therefore, the 'count' should always be '1'
             */
            String fhirJson = randomPatientService.createPatientBundleJson(params.getZipcode(), params.getRadius());
            kafkaPersonService.produceAsyncMessage(fhirJson);
        }
    }
}
