package org.jaroof.fh8.randomzier.repository;

import org.jaroof.fh8.domain.lookup.Zipcode;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ZipcodesRepository extends CrudRepository<Zipcode, Integer> {

    @Query(value="SELECT * FROM zipcodes ORDER BY RAND() LIMIT :count", nativeQuery = true)
    List<Zipcode> findRandomNativeMysql(@Param("count")int count);

    @Query(value = "SELECT *, (6371 * acos(cos( radians( :latitude ) ) * cos( radians( latitude ) ) * cos(radians( longitude ) - radians( :longitude )) +  sin(radians(:latitude)) * sin(radians(latitude)))) distance FROM zipcodes HAVING distance < :distance ORDER BY distance LIMIT 100", nativeQuery = true)
    List<Zipcode> findByCoordinates(@Param("latitude") double latitude, @Param("longitude") double longitude,
                                    @Param("distance") int distance);

    Zipcode findFirstByZipcode(Integer zipcode);
}
