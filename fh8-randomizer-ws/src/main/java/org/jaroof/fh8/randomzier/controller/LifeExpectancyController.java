package org.jaroof.fh8.randomzier.controller;

import org.jaroof.fh8.domain.lookup.LifeExpectancy;
import org.jaroof.fh8.dto.RandomDeceasedParams;
import org.jaroof.fh8.dto.Success;
import org.jaroof.fh8.randomzier.service.LifeExpectancyService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("v1/life-expectency")
public class LifeExpectancyController {

    private LifeExpectancyService lifeExpectancyService;

    public LifeExpectancyController(LifeExpectancyService lifeExpectancyService) {
        this.lifeExpectancyService = lifeExpectancyService;
    }

    @GetMapping("/years-remaining")
    public LifeExpectancy yearsRemaining(@RequestParam(value = "age") Integer age) {
        return lifeExpectancyService.getLifeExpectancyByAge(age);
    }

    /**
     * Returns true/false based on a random chance for a deceased event
     * @param randomDeceasedParams Parameters used to calculate the deceased event
     * @return
     */
    @GetMapping("/random-deceased")
    public Success randomDeceased(RandomDeceasedParams randomDeceasedParams) {
        Double modifier = randomDeceasedParams.getModifier();
        if (randomDeceasedParams.getModifier() == null || randomDeceasedParams.getModifier() <= 0) {
            modifier = Double.valueOf(1);
        }
        boolean deceasedEvent = lifeExpectancyService.isDeceasedEvent(randomDeceasedParams.getAge(),
                randomDeceasedParams.getSex(), modifier);
        Success val = new Success();
        val.setValue(deceasedEvent);
        return val;
    }
}
