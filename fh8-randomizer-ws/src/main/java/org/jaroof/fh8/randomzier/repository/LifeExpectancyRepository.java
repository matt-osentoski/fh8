package org.jaroof.fh8.randomzier.repository;

import org.jaroof.fh8.domain.lookup.LifeExpectancy;
import org.springframework.data.repository.CrudRepository;

public interface LifeExpectancyRepository extends CrudRepository<LifeExpectancy, Long> {

    LifeExpectancy findFirstByAge(int age);
}
