package org.jaroof.fh8.randomzier.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
@Slf4j
public class KafkaPersonService {

    private KafkaTemplate<String, String> kafkaTemplate;
    private String topic;

    public KafkaPersonService(KafkaTemplate<String, String> kafkaTemplate,
                              @Value("${kafka.topic.insert.person}") String topic) {
        this.kafkaTemplate = kafkaTemplate;
        this.topic = topic;
    }

    public void produceMessage(String message) {
        log.debug(String.format("Producing message -> %s", message));
        this.kafkaTemplate.send(topic, message);
    }

    @Async
    public void produceAsyncMessage(String message) {
        log.debug(String.format("Producing message -> %s", message));
        ListenableFuture<SendResult<String, String>> future = this.kafkaTemplate.send(topic, message);
        future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {

            @Override
            public void onSuccess(final SendResult<String, String> message) {
                log.info("sent message= " + message + " with offset= " + message.getRecordMetadata().offset());
            }

            @Override
            public void onFailure(final Throwable throwable) {
                log.error("unable to send message= " + message, throwable);
            }
        });
    }
}
