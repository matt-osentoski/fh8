package org.jaroof.fh8.randomzier.repository;

import org.jaroof.fh8.domain.lookup.AgeDistribution;
import org.springframework.data.repository.CrudRepository;

public interface AgeDistributionRepository extends CrudRepository<AgeDistribution, Long> {

}
