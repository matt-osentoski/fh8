package org.jaroof.fh8.randomzier.repository;

import org.jaroof.fh8.domain.lookup.PersonName;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PersonRepository extends CrudRepository<PersonName, Integer> {

    @Query(value="SELECT * FROM actor_names ORDER BY RAND() LIMIT :count", nativeQuery = true)
    List<PersonName> findRandomNativeMysql(@Param("count")int count);
}
