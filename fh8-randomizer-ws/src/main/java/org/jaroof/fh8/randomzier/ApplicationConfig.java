package org.jaroof.fh8.randomzier;

import ca.uhn.fhir.context.FhirContext;
import org.apache.kafka.clients.admin.NewTopic;
import org.jaroof.fh8.domain.lookup.AgeDistribution;
import org.jaroof.fh8.domain.lookup.LifeExpectancy;
import org.jaroof.fh8.randomzier.repository.AgeDistributionRepository;
import org.jaroof.fh8.randomzier.repository.LifeExpectancyRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
@EnableSwagger2
public class ApplicationConfig {

    @Value("${kafka.topic.update.person}")
    private String updatePersonTopicName;

    @Value("${kafka.topic.insert.person}")
    private String insertPersonTopicName;

    private LifeExpectancyRepository lifeExpectancyRepository;
    private AgeDistributionRepository ageDistributionRepository;

    public ApplicationConfig(LifeExpectancyRepository lifeExpectancyRepository,
                             AgeDistributionRepository ageDistributionRepository) {
        this.lifeExpectancyRepository = lifeExpectancyRepository;
        this.ageDistributionRepository = ageDistributionRepository;
    }

    @Bean
    public FhirContext fhirContext() {
        return FhirContext.forR4();
    }

    @Bean
    public Docket projectApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.regex("/v1.*"))
                .build();
    }

    /**
     * Cache all life expectancy values into a Map with age (integer) as the key and LifeExpectancy object as the value
     * @return
     */
    @Bean
    public Map<Integer, LifeExpectancy> lifeExpectancyMap() {
        Map<Integer, LifeExpectancy> map = new HashMap<>();
        Iterable<LifeExpectancy> lifeExpectancyIter = lifeExpectancyRepository.findAll();
        lifeExpectancyIter.forEach(l -> map.put(l.getAge(), l));
        return map;
    }

    @Bean
    public List<Integer> ageDistributionList() {
        List<Integer> ageList = new ArrayList<>();
        Iterable<AgeDistribution> ageIter = ageDistributionRepository.findAll();
        ageIter.forEach(a -> ageList.add(a.getAge()));
        return ageList;
    }

    /**
     * Creates the insert Person topic. This is used for bulk inserts without the upsert checking overhead.
     * @return
     */
    @Bean
    public NewTopic insertPersonTopic() {
        return TopicBuilder.name(insertPersonTopicName)
                .partitions(10)
                .replicas(3)
                .build();
    }

    /**
     * Creates the update Person topic.
     * @return
     */
    @Bean
    public NewTopic updatePersonTopic() {
        return TopicBuilder.name(updatePersonTopicName)
                .partitions(10)
                .replicas(3)
                .build();
    }
}
