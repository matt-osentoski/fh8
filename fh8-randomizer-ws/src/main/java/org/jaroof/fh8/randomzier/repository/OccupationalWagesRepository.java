package org.jaroof.fh8.randomzier.repository;

import org.jaroof.fh8.domain.lookup.OccupationalWage;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OccupationalWagesRepository extends CrudRepository<OccupationalWage, Integer> {

    @Query(value="SELECT * FROM occupational_wages ORDER BY RAND() LIMIT :count", nativeQuery = true)
    List<OccupationalWage> findRandomNativeMysql(@Param("count")int count);
}
