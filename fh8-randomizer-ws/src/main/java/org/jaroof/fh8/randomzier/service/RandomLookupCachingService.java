package org.jaroof.fh8.randomzier.service;

import lombok.extern.slf4j.Slf4j;
import org.jaroof.fh8.domain.lookup.OccupationalWage;
import org.jaroof.fh8.domain.lookup.PersonName;
import org.jaroof.fh8.domain.lookup.StreetNames;
import org.jaroof.fh8.domain.lookup.Zipcode;
import org.jaroof.fh8.randomzier.repository.OccupationalWagesRepository;
import org.jaroof.fh8.randomzier.repository.PersonRepository;
import org.jaroof.fh8.randomzier.repository.StreetNamesRepository;
import org.jaroof.fh8.randomzier.repository.ZipcodesRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Generating random objects from a lookup table is an expensive operation when performed one-by-one.
 * This class cache's groups of random objects and rebuilds the cache as needed. Cache size is adjustable.
 */
@Service
@Slf4j
public class RandomLookupCachingService {


    private int cacheSize;
    private OccupationalWagesRepository occupationalWagesRepository;
    private StreetNamesRepository streetNamesRepository;
    private ZipcodesRepository zipcodesRepository;
    private PersonRepository personRepository;

    private Deque<OccupationalWage> occupationalWages = new LinkedList<>();
    private Deque<StreetNames> streetNames = new LinkedList<>();
    private Deque<Zipcode> zipcodes = new LinkedList<>();
    private Deque<PersonName> people = new LinkedList<>();

    public RandomLookupCachingService(OccupationalWagesRepository occupationalWagesRepository,
                                      StreetNamesRepository streetNamesRepository,
                                      ZipcodesRepository zipcodesRepository,
                                      PersonRepository personRepository,
                                      @Value("${lookup.random.cache.size}") int cacheSize) {
        this.occupationalWagesRepository = occupationalWagesRepository;
        this.streetNamesRepository = streetNamesRepository;
        this.zipcodesRepository = zipcodesRepository;
        this.personRepository = personRepository;
        this.cacheSize = cacheSize;
    }

    public OccupationalWage getRandomOccupationalWage() {

        if (occupationalWages.size() == 0) {
            occupationalWages.addAll(occupationalWagesRepository.findRandomNativeMysql(cacheSize));
        }
        OccupationalWage occupationalWage = occupationalWages.pop();
        return occupationalWage;
    }

    public StreetNames getRandomStreetName() {
        if (streetNames.size() == 0) {
            streetNames.addAll(streetNamesRepository.findRandomNativeMysql(cacheSize));
        }
        StreetNames streetName = streetNames.pop();
        return streetName;
    }

    public Zipcode getRandomZipcode(int zipcodeParam, int radius) {
        if (zipcodes.size() == 0) {
            Zipcode zipcodeObj = zipcodesRepository.findFirstByZipcode(zipcodeParam);
            List<Zipcode> zipcodesTmp = zipcodesRepository.findByCoordinates(
                    zipcodeObj.getLatitude(), zipcodeObj.getLongitude(), radius);
            Random rand = new Random();
            for (int x = 0; x<cacheSize; x++) {
                int randIdx = rand.nextInt(zipcodesTmp.size() - 0);
                Zipcode z = zipcodesTmp.get(randIdx);
                zipcodes.add(z);
            }
        }
        Zipcode zipcode = zipcodes.pop();
        return zipcode;
    }

    public PersonName getRandomPerson() {
        if (people.size() == 0) {
            people.addAll(personRepository.findRandomNativeMysql(cacheSize));
        }
        PersonName personName = people.pop();
        return personName;
    }
}
