package org.jaroof.fh8.randomzier.service;

import org.jaroof.fh8.Constants;
import org.jaroof.fh8.domain.lookup.LifeExpectancy;
import org.jaroof.fh8.enums.SexEnum;
import org.jaroof.fh8.randomzier.repository.LifeExpectancyRepository;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class LifeExpectancyService {

    private Map<Integer, LifeExpectancy> lifeExpectancyMap;

    public LifeExpectancyService(Map<Integer, LifeExpectancy> lifeExpectancyMap) {
        this.lifeExpectancyMap = lifeExpectancyMap;
    }

    public LifeExpectancy getLifeExpectancyByAge(int age) {
        return lifeExpectancyMap.get(age);
    }

    public boolean isDeceasedEvent(int age, String sex, double modifier) {
        boolean isDeceased = false;
        LifeExpectancy lifeExcpectancy = this.getLifeExpectancyByAge(age);
        if (sex.equals(SexEnum.FEMALE.getValue())) {
            isDeceased = this.randomDeceasedEventByYears(lifeExcpectancy.getFemaleYears(), modifier);
        } else if (sex.equals(SexEnum.MALE.getValue())) {
            isDeceased = this.randomDeceasedEventByYears(lifeExcpectancy.getMaleYears(), modifier);
        } else {  // If unknown use the Male values
            isDeceased = this.randomDeceasedEventByYears(lifeExcpectancy.getMaleYears(),modifier);
        }

        return isDeceased;
    }

    /**
     * Calculates the number of projected days left of life based on years remaining.  A random int value is
     * then computed using the remainingDays as an upper range and '1' as the lower range.  If '1' is returned, then
     * a the patient has had a 'deceasedEvent'
     * @param yearsRemaining years of projected life left based on actuarial tables.
     * @param modifier A modifier that can modify potential projected days of life left.  For example, '0.5' would half the projected time.  The value cannot exceept 1 (100%)
     * @return
     */
    protected boolean randomDeceasedEventByYears(double yearsRemaining, double modifier) {
        boolean isDeceased = false;
        if (modifier <= 0) {
            modifier = 1;
        }
        int daysRemaining = (int) (yearsRemaining * Constants.DAYS_IN_YEAR * modifier);
        int randomNum = ThreadLocalRandom.current().nextInt(1, daysRemaining + 1);
        if (randomNum == 1) {
            isDeceased = true;
        }
        return isDeceased;
    }
}
