package org.jaroof.fh8.randomzier.service;

import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.StringType;
import org.jaroof.fh8.domain.lookup.OccupationalWage;
import org.jaroof.fh8.utils.FhirUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.jaroof.fh8.Constants.*;
import static org.jaroof.fh8.utils.FhirUtils.createBaseMeta;

@Service
public class EmploymentObservationService {

    private RandomLookupCachingService randomLookupCachingService;

    public EmploymentObservationService(RandomLookupCachingService randomLookupCachingService) {
        this.randomLookupCachingService = randomLookupCachingService;
    }

    public List<Observation> getRandomEmploymentObservations(Patient patient) {
        List<Observation> observations = new ArrayList<>();
        OccupationalWage occupation = randomLookupCachingService.getRandomOccupationalWage();
        observations.add(getOccupation(patient, occupation));
        observations.add(getSalary(patient, occupation));

        return observations;
    }

    public List<Observation> getStudentEmploymentObservations(Patient patient) {
        return getNoWageEmploymentObservations(patient, "Student");
    }

    public List<Observation> getRetiredEmploymentObservations(Patient patient) {
        return getNoWageEmploymentObservations(patient, "Retired");
    }

    protected List<Observation> getNoWageEmploymentObservations(Patient patient, String title) {
        List<Observation> observations = new ArrayList<>();
        OccupationalWage occupation = new OccupationalWage();
        occupation.setId(1);
        occupation.setTitle(title);
        occupation.setWage(0);
        observations.add(getOccupation(patient, occupation));
        observations.add(getSalary(patient, occupation));
        return observations;
    }

    protected Observation getBaseObservation(Patient patient) {
        Observation observation = new Observation();
        observation.setId(UUID.randomUUID().toString());
        observation.setIdentifier(FhirUtils.createSingleUuidIdentifier());
        observation.setMeta(createBaseMeta());
        observation.setSubject(getPatientReference(patient));
        observation.setStatus(Observation.ObservationStatus.REGISTERED);
        return observation;
    }


    protected Observation getOccupation(Patient patient, OccupationalWage occupationalWage) {
        Observation occupation = getBaseObservation(patient);
        occupation.setCode(FhirUtils.createSingleCodeableConcept(CODING_SYSTEM_SNOMED,
                SNOMED_CODE_OCCUPATION, "Occupation"));
        StringType occupationValue = new StringType(occupationalWage.getTitle());
        occupation.setValue(occupationValue);
        return occupation;
    }

    protected Observation getSalary(Patient patient, OccupationalWage occupationalWage) {
        Observation salary = getBaseObservation(patient);
        salary.setCode(FhirUtils.createSingleCodeableConcept(
                CODING_SYSTEM_OBSERVATION_VALUE_HL7_V3, HL7_V3_CODE_SALARY, "employee salary"));
        StringType salaryValue = new StringType(Double.toString(occupationalWage.getWage()));
        salary.setValue(salaryValue);
        return salary;
    }

    protected Reference getPatientReference(Patient patient) {
        Reference ref = new Reference();
        // NOTE: IMPORTANT!!! When using a Bundle transaction, referenced resources should use
        // setResource or setIdentifier and NOT setReference with a Patient identifier, the transaction gets confused
        // at the FHIR server and generates empty parent resources, which is VERY buggy

        // Uncomment set Identifier to change the reference type. Use setResource for speed.
        //ref.setIdentifier(patient.getIdentifier().get(0));
        ref.setResource(patient);
        return ref;
    }


}
