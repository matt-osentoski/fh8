# Data sources

## Patient Names
Actor information used for name, birthdate and gender information are gathered from
IMDB via the link below:

[https://datasets.imdbws.com/](https://datasets.imdbws.com/)

>(NOTE: Some fields may be missing in newer versions of this dataset. Another dataset may
>have to be explored)

## Age Distribution
I manually pieced together a frequency table for age. This was accomplished by taking
an example distribution of ages then, creating a table:
https://www.shmoop.com/study-guides/biology/ecology/age-structure

This is necessary because the distribution of ages is far from "normal" and varies by region. 

## Common Codes
This is a manually created dataset based on ICD-10 and SNOMED codes. I then looked up
common codes for an urgent care and pieced the dataset together.  You will need access to 
ICD-10 and SNOMED codes, which could be an issue.

## Occupation Wages
This dataset comes from the CDC. I'm using a variation of PHVS_Occupation_CDC_Census<YEAR> dataset.
Often this is older data, so, you may have to add an inflation-based multiplier to get more accurate
salary values.

[https://phinvads.cdc.gov/vads/SearchValueSets_search.action?searchOptions.searchText=occupation](https://phinvads.cdc.gov/vads/SearchValueSets_search.action?searchOptions.searchText=occupation)

## Street Names
Common street names comes from data.gov dataset.  

[https://catalog.data.gov/dataset/street-names](https://catalog.data.gov/dataset/street-names)

## Zipcode information
Zipcodes including coordinates and population information is from Geonames

[http://download.geonames.org/export/zip/](http://download.geonames.org/export/zip/)

>(NOTE: This set seems to be missing the population property. Either remove this field,
>or leave the value empty. Another dataset may be considered, as well.)
