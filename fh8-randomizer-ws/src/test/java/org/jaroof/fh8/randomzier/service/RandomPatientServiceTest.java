package org.jaroof.fh8.randomzier.service;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.jaroof.fh8.Constants.MAX_AGE;
import static org.junit.jupiter.api.Assertions.*;

public class RandomPatientServiceTest {

    private RandomPatientService service;
    private List<Integer> ageList = new ArrayList<>();

    @BeforeAll
    static void initAll() {}

    @BeforeEach
    void init() throws Exception {
        ageList.add(0);
        ageList.add(0);
        ageList.add(0);
        ageList.add(1);
        ageList.add(12);
        ageList.add(25);
        ageList.add(49);
        ageList.add(65);
        ageList.add(99);
        service = new RandomPatientService(null, null, null, ageList);
    }

    @Test
    public void getRandomAddressNumber() {
        for (int x = 0; x<100; x++) {
            int num = service.getRandomAddressNumber();
            assertTrue(num < 99999 && num > 0);
        }
    }

    @Test
    public void splitNameElements() {
        RandomPatientService.NamePairs namePairs = service.splitNameElements("John A Smith");
        assertEquals(2, namePairs.given.size());
        assertEquals("John", namePairs.given.get(0).asStringValue());
        assertEquals("A", namePairs.given.get(1).asStringValue());
        assertEquals("Smith", namePairs.family);
        assertNotEquals("asdf", namePairs.family);

        namePairs = service.splitNameElements("Cher");
        assertNull(namePairs.family);
        assertEquals(1, namePairs.given.size());
        assertEquals("Cher", namePairs.given.get(0).asStringValue());

        namePairs = service.splitNameElements("Tyler Durden");
        assertEquals(1, namePairs.given.size());
        assertEquals("Tyler", namePairs.given.get(0).asStringValue());
        assertEquals("Durden", namePairs.family);
        assertNotEquals("asdf", namePairs.family);


    }

    @Test
    public void getRandomBirthdate() {
        int currentYear =  Calendar.getInstance().get(Calendar.YEAR);
        Date randomBirthdate = service.getRandomBirthdate();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(randomBirthdate);
        int year = calendar.get(Calendar.YEAR);
        assertTrue(year >= currentYear - MAX_AGE && year <= currentYear);
    }
}