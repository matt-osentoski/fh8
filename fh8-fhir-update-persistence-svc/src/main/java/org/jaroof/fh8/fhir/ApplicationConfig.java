package org.jaroof.fh8.fhir;

import ca.uhn.fhir.context.FhirContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class ApplicationConfig {

    @Bean
    public FhirContext fhirContext() {
        return FhirContext.forR4();
    }
}
