# fh8-fhir-update-persistence-svc
This module is a Microservice that consumes FHIR Patient (and related) resources from a Kafka topic then
inserts them into a FHIR server.  All FHIR resources are assumed to be packaged in a bundle
during ingestion.

>(NOTE: This service performs Upsert operations, designed to be idempotent.  For bulk inserts
>a different service should be used that doesn't have the upsert check. Otherwise, performance
>won't be quite as good)

## Application Flow
- Consume a FHIR Bundle from a Kafka topic.
- Extract individual FHIR resources from the bundle
- POST the resources to a compliant FHIR server to act as the 'source of truth' for this
data.

## Manually build the module
This module is normally build from the parent POM, but can be built individually.
```
mvn clean install
```

