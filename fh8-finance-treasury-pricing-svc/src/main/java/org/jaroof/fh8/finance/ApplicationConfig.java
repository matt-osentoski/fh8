package org.jaroof.fh8.finance;

import org.apache.kafka.clients.admin.NewTopic;
import org.jaroof.fh8.domain.finance.Treasury;
import org.jaroof.fh8.finance.domain.TreasuryRange;
import org.jaroof.fh8.random.Dice;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class ApplicationConfig {
    @Value("${kafka.topic.treasury.rates}")
    private String treasuryTopic;

    @Value("${treasury.new.rate.low}")
    private String newRateLowStr;

    @Value("${treasury.new.rate.high}")
    private String newRateHighStr;

    @Value("${dice.size}")
    private String diceSizeStr;

    @Value("${dice.critical.hit}")
    private String diceCriticalHitStr;

    @Bean
    public Treasury lastTreasuryValue() {
        return new Treasury();
    }

    @Bean
    public Dice dice() {
        Dice dice = new Dice();
        dice.setSize(Integer.parseInt(diceSizeStr));
        dice.setCriticalHit(Integer.parseInt(diceCriticalHitStr));
        return dice;
    }

    @Bean
    public TreasuryRange treasuryRange() {
        TreasuryRange treasuryRange = new TreasuryRange();
        treasuryRange.setNewRateLow(Long.parseLong(newRateLowStr));
        treasuryRange.setNewRateHigh(Long.parseLong(newRateHighStr));
        return treasuryRange;
    }

    /**
     * Creates a new topic
     * @return
     */
    @Bean
    public NewTopic treasuryTopic() {
        return TopicBuilder.name(treasuryTopic)
                .partitions(1)
                .replicas(3)
                // Never delete any messages in this topic
                .config("retention.ms", "-1")
                .config("retention.bytes", "-1")
                .build();
    }
}
