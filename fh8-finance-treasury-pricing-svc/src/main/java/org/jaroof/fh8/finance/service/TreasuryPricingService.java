package org.jaroof.fh8.finance.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.ParetoDistribution;
import org.jaroof.fh8.domain.finance.Treasury;
import org.jaroof.fh8.finance.domain.TreasuryRange;
import org.jaroof.fh8.random.Dice;
import org.jaroof.fh8.utils.NumberUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;
import java.util.Random;

/**
 * This service manages the pricing of a Large Cap index fund (stock)
 */
@Service
@Slf4j
public class TreasuryPricingService {

    private final static int ROUNDED_DECIMAL_PLACES = 4;

    private Treasury lastTreasuryValue;
    private TreasuryRange treasuryRange;
    private Dice dice;

    public TreasuryPricingService(Treasury lastTreasuryValue, TreasuryRange treasuryRange, Dice dice) {
        this.lastTreasuryValue = lastTreasuryValue;
        this.treasuryRange = treasuryRange;
        this.dice = dice;
    }

    /**
     * Generates a new random price for a specific day (the epoch value).
     * @param epoch The epoch value that corresponds to a day
     * @return A new Treasury object containing a randomly generated closing percentage, specifically
     */
    public Treasury generateRate(long epoch) {
        Treasury treasury = null;
        if (lastTreasuryValue == null || lastTreasuryValue.getC30year() == 0) {
            treasury = this.generateFirstTreasury(epoch);
        } else {
            treasury = this.generateRandomRate(epoch);
        }
        lastTreasuryValue = treasury;
        return treasury;
    }

    /**
     * Randomly generates the closing rate of a treasury based off the last rate and custom algorithms using different
     * distributions to vary the thickness of the tails.
     *
     * @param epoch
     */
    protected Treasury generateRandomRate(long epoch) {
        Treasury treasury = new Treasury();
        treasury.setPriceDate(new Date(epoch));
        treasury.setC1year(NumberUtils.roundDouble(this.randomRateAlgo(lastTreasuryValue.getC1year())
                ,ROUNDED_DECIMAL_PLACES));
        treasury.setC2year(NumberUtils.roundDouble(this.randomRateAlgo(lastTreasuryValue.getC2year())
                ,ROUNDED_DECIMAL_PLACES));
        treasury.setC3year(NumberUtils.roundDouble(this.randomRateAlgo(lastTreasuryValue.getC3year())
                ,ROUNDED_DECIMAL_PLACES));
        treasury.setC5year(NumberUtils.roundDouble(this.randomRateAlgo(lastTreasuryValue.getC5year())
                ,ROUNDED_DECIMAL_PLACES));
        treasury.setC7year(NumberUtils.roundDouble(this.randomRateAlgo(lastTreasuryValue.getC7year())
                ,ROUNDED_DECIMAL_PLACES));
        treasury.setC10year(NumberUtils.roundDouble(this.randomRateAlgo(lastTreasuryValue.getC10year())
                ,ROUNDED_DECIMAL_PLACES));
        treasury.setC20year(NumberUtils.roundDouble(this.randomRateAlgo(lastTreasuryValue.getC20year())
                ,ROUNDED_DECIMAL_PLACES));
        treasury.setC30year(NumberUtils.roundDouble(this.randomRateAlgo(lastTreasuryValue.getC30year())
                ,ROUNDED_DECIMAL_PLACES));
        return treasury;
    }

    /**
     * This method contains the algorithm used for pricing a stock (index in this case). The algo is dependent on
     * the last closing price to base the current price from.
     * @param bondPercent The previous bond price.
     * @return A new closing price that is randomly generated
     */
    protected double randomRateAlgo(double bondPercent) {
        double newPercent = bondPercent;
        boolean criticalHit = false;
        Random rand = new Random();
        double percent = 0;
        int rolledDie = rand.nextInt(dice.getSize()) + 1;
        // If a critical hit is rolled, use a more extreme distribution (Pareto, Cauchy, etc.)
        if (rolledDie == dice.getCriticalHit()) {
            ParetoDistribution p = new ParetoDistribution(1,2.25);
            percent = p.sample() - 1; // subtract one to make the starting range closer to zero.

            // Pareto doesn't have positive / negative values like a normal distribution
            // so, randomly make the percent negative
            if (rand.nextBoolean()) {
                percent = percent * -1;
            }
            log.info("Critical Hit percent: " + percent);
        } else {
            NormalDistribution n = new NormalDistribution();
            percent = n.sample();
        }

        newPercent = bondPercent + (bondPercent * (percent/100));
        return newPercent;
    }

    /**
     * This method is called if the Kafka topic was empty.  This is the starting Stock object which will
     * seed this index and the Kafka topic.  Since pricing is dependent on the last closing price, this first
     * Stock has to have a price range that would make sense for this index. Additional prices will then fluctuate
     * randomly
     *
     * @param epoch
     * @return
     */
    protected Treasury generateFirstTreasury(long epoch) {
        Treasury treasury = new Treasury();
        treasury.setPriceDate(new Date(epoch));
        Random rand = new Random();
        double randomCLosingPrice =
                treasuryRange.getNewRateLow() + (treasuryRange.getNewRateHigh() - treasuryRange.getNewRateLow()) *
                        rand.nextDouble();
        treasury.setC30year(NumberUtils.roundDouble(randomCLosingPrice,ROUNDED_DECIMAL_PLACES));

        // We start with the 30 year rate, then divide by 30 to get the annual rate. Each rate can then be determined by
        // multiplying by this base number
        double singleYearRate = randomCLosingPrice / 30;
        treasury.setC1year(NumberUtils.roundDouble(singleYearRate * 1,ROUNDED_DECIMAL_PLACES));
        treasury.setC2year(NumberUtils.roundDouble(singleYearRate * 2,ROUNDED_DECIMAL_PLACES));
        treasury.setC3year(NumberUtils.roundDouble(singleYearRate * 3,ROUNDED_DECIMAL_PLACES));
        treasury.setC5year(NumberUtils.roundDouble(singleYearRate * 5,ROUNDED_DECIMAL_PLACES));
        treasury.setC7year(NumberUtils.roundDouble(singleYearRate * 7,ROUNDED_DECIMAL_PLACES));
        treasury.setC10year(NumberUtils.roundDouble(singleYearRate * 10,ROUNDED_DECIMAL_PLACES));
        treasury.setC20year(NumberUtils.roundDouble(singleYearRate * 20,ROUNDED_DECIMAL_PLACES));

        return treasury;
    }

    /**
     * Set the lastStockPrice bean (singleton) based on the JSON representation coming from the Kafka topic.
     * @param jsonStock
     */
    public void setLastRateFromJson(String jsonStock) {
        Treasury treasury = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            treasury = objectMapper.readValue(jsonStock, Treasury.class);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        lastTreasuryValue = treasury;
    }
}
