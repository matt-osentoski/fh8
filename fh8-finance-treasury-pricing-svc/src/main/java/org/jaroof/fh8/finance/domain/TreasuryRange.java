package org.jaroof.fh8.finance.domain;

import lombok.Data;

@Data
public class TreasuryRange {
    private long newRateLow;
    private long newRateHigh;
}
