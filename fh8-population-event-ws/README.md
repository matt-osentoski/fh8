# fh8-population-event-ws
This module is a Web Service that randomly grabs groups of living people.  These people are then produced on the 
'fh8.population.person.json' topic. This happens continuously in a loop every X number of ms. The Web Service also
checks that the fh8-time-loop-ws is running, by querying an endpoint. If the time service is not running, this
service will not process any new requests, since time has effectively stopped.

## Application Flow
- A flag that determines if the fh8-time-loop-ws is running is set to 'false' as its default value. (isTimeRunning)
- Every X time interval, a loop checks the time loop Web Service to see if it is running. If it is the 'isTimeRunning'
boolean is set to `true`
- A loop checks if 'isTimeRunning' is set to `true`. If the flag is true a group of living people randomly queried
from the population database table.
- The random living people are then produced into the 'fh8.population.person.json' topic for further event processing.

### External service calls
Life Expectancy is handled by a Kogito DMN service that uses actuary tables to
determine life expectancy.

**Project:**  fh8-quarkus/fh8-life-event-dmn-ws
**Endpoint:** http://fh8-life-event-dmn-ws.fh8.svc.cluster.local:8080/life-expectancy
**Payload:** `{"age": 32, "sex": male, "modifier": 1}`



## Manually build the module
This module is normally build from the parent POM, but can be built individually.
```
mvn clean install
```

