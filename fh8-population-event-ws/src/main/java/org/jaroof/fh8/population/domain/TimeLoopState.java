package org.jaroof.fh8.population.domain;

import lombok.Data;

@Data
public class TimeLoopState {
    private boolean active;
}
