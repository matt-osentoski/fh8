package org.jaroof.fh8.population.service;

import lombok.extern.slf4j.Slf4j;
import org.jaroof.fh8.dto.VirtualTime;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class KafkaConsumerTimeService {

    private VirtualTime virtualTime;

    public KafkaConsumerTimeService(VirtualTime virtualTime) {
        this.virtualTime = virtualTime;
    }

    /**
     * Consume Epoch time messages that contain the current virtual time.
     *
     * NOTE: We really don't want to use a group here, so generate a random group for each consumer, via UUID
     * @param message
     */
    @KafkaListener(topics = "#{'${kafka.topic.time.epoch}'.split(',')}", groupId = "time-grp-#{ T(java.util.UUID).randomUUID().toString() }")
    public void consume(String message) {
        log.info(String.format("$$ -> Consumed Message -> %s",message));
        long epoch = Long.parseLong(message);
        virtualTime.setEpochTime(epoch);
    }

}
