package org.jaroof.fh8.population.schedule;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.jaroof.fh8.domain.population.Person;
import org.jaroof.fh8.dto.VirtualTime;
import org.jaroof.fh8.population.domain.TimeLoopState;
import org.jaroof.fh8.population.repository.PersonRepository;
import org.jaroof.fh8.population.service.KafkaProducerPopulationService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Slf4j
@Component
public class PopulationEventSchedule {

    private String populationMaxBatchSizeStr;
    private TimeLoopState timeLoopState;
    private VirtualTime virtualTime;
    private PersonRepository personRepository;
    private KafkaProducerPopulationService kafkaProducerPopulationService;

    public PopulationEventSchedule(@Value("${population.event.max.batch.size}") String populationMaxBatchSizeStr,
                                   TimeLoopState timeLoopState, VirtualTime virtualTime,
                                   PersonRepository personRepository,
                                   KafkaProducerPopulationService kafkaProducerPopulationService) {
        this.populationMaxBatchSizeStr = populationMaxBatchSizeStr;
        this.timeLoopState = timeLoopState;
        this.virtualTime = virtualTime;
        this.personRepository = personRepository;
        this.kafkaProducerPopulationService = kafkaProducerPopulationService;
    }

    @Scheduled(fixedRateString = "${population.event.loop.fixed.rate}")
    public void eventLoop() {
        // Run the current event if the Time service is activated and running
        if (timeLoopState.isActive()) {
            List<Person> livingPopulation = personRepository.findRandomNativeMysql(Integer.parseInt(populationMaxBatchSizeStr));
            livingPopulation.forEach(p -> this.sendMessage(p));
        }
    }

    protected void sendMessage(Person person) {
        person.setVirtualDate(new Date(virtualTime.getEpochTime()));
        ObjectMapper objectMapper = new ObjectMapper();
        String personJson = null;
        try {
            personJson = objectMapper.writeValueAsString(person);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        kafkaProducerPopulationService.produceAsyncMessage(personJson);
    }
}
