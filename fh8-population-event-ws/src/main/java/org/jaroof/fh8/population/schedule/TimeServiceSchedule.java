package org.jaroof.fh8.population.schedule;

import lombok.extern.slf4j.Slf4j;
import org.jaroof.fh8.dto.ApplicationState;
import org.jaroof.fh8.population.domain.TimeLoopState;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
public class TimeServiceSchedule {

    private TimeLoopState timeLoopState;
    private RestTemplate restTemplate;
    private String uri;

    public TimeServiceSchedule(@Value("${is.time.loop.running.service.uri}") String uri, TimeLoopState timeLoopState, RestTemplate restTemplate) {
        this.uri = uri;
        this.timeLoopState = timeLoopState;
        this.restTemplate = restTemplate;
    }

    /**
     * Check to see if the timeloop service is currently running then randomly create population events
     * TODO: Investigate replacing this with Kafka.
     */
    @Scheduled(fixedDelayString = "${check.time.loop.initial.delay}", initialDelayString = "${check.time.loop.fixed.delay}")
    public void checkTimeLoopStatus() {
        ApplicationState applicationState = restTemplate.getForObject(uri, ApplicationState.class);
        if (!this.timeLoopState.isActive()) {
            log.info("Waiting for the Time Loop Service to be activated...");
        }
        if (this.timeLoopState.isActive() != applicationState.isActive()) {
            log.info("Time Loop Service - Application State changed: " + applicationState.isActive());
        }
        this.timeLoopState.setActive(applicationState.isActive());
    }
}
