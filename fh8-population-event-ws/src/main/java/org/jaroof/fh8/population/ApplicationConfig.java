package org.jaroof.fh8.population;

import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.Consumer;
import org.jaroof.fh8.dto.VirtualTime;
import org.jaroof.fh8.population.domain.TimeLoopState;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ApplicationConfig {

    @Value("${kafka.topic.population.person}")
    private String populationTopicName;

    @Bean
    public TimeLoopState timeLoopState() {
        return new TimeLoopState();
    }

    @Bean
    public VirtualTime virtualTime() { return new VirtualTime(); }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    public HttpHeaders applicationJsonHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    /**
     * Creates a new topic
     * @return
     */
    @Bean
    public NewTopic populationTopic() {
        return TopicBuilder.name(populationTopicName)
                .partitions(10)
                .replicas(3)
                .build();
    }
}
