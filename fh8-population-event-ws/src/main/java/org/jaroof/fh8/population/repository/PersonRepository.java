package org.jaroof.fh8.population.repository;

import org.jaroof.fh8.domain.population.Person;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PersonRepository extends CrudRepository<Person, Integer> {

    @Query(value="SELECT * FROM person WHERE deceased = 0 ORDER BY RAND() LIMIT :count", nativeQuery = true)
    List<Person> findRandomNativeMysql(@Param("count")int count);

}
