# DEPRECATED - fh8-population-list-svc

>(DEPRECATED: This service has been replaced by `fh8-population-event-ws` which is not tightly coupled to the 
> main time loop and also is horizontally scalable.)

This module is a Microservice that consumes time messages from a kafka topic.  Each time message
is a time snapshot in the future (a tick). With each tick of the time topic, query a database for data related to living people and
creates JSON objects which are then placed into a kafka topic for updates and processing.  This data is a denormalized version of FHIR data for faster/easier retrieval.  

## Application Flow
- Consume time messages from a Kafka topic. Each message is a time unit in the future (tick)
- Query a database and retrieve living person rows
- Convert these objects into JSON
- Using a Kafka producer, place the person JSON objects onto a topic.


## Manually build the module
This module is normally build from the parent POM, but can be built individually.
```
mvn clean install
```

