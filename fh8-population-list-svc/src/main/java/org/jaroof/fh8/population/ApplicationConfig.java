package org.jaroof.fh8.population;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class ApplicationConfig {

    @Value("${kafka.topic.population.person}")
    private String populationTopicName;

    /**
     * Creates a new topic
     * @return
     */
    @Bean
    public NewTopic populationTopic() {
        return TopicBuilder.name(populationTopicName)
                .partitions(10)
                .replicas(3)
                .build();
    }
}
