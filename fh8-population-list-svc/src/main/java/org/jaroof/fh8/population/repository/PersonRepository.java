package org.jaroof.fh8.population.repository;

import org.jaroof.fh8.domain.population.Person;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PersonRepository extends CrudRepository<Person, Integer> {

    List<Person> findByDeceasedFalse();
}
