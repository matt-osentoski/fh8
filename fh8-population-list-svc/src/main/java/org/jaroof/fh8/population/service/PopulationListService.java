package org.jaroof.fh8.population.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.jaroof.fh8.domain.population.Person;
import org.jaroof.fh8.population.repository.PersonRepository;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class PopulationListService {

    private PersonRepository personRepository;
    private KafkaProducerPersonService kafkaProducerPersonService;

    public PopulationListService(PersonRepository personRepository,
                                 KafkaProducerPersonService kafkaProducerPersonService) {
        this.personRepository = personRepository;
        this.kafkaProducerPersonService = kafkaProducerPersonService;
    }

    public void processLivingPopulation(long epochTime) {
        List<Person> people = personRepository.findByDeceasedFalse();
        for (Person person: people) {
            person.setVirtualDate(new Date(epochTime));
            ObjectMapper objectMapper = new ObjectMapper();
            String personJson = null;
            try {
                personJson = objectMapper.writeValueAsString(person);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            kafkaProducerPersonService.produceAsyncMessage(personJson);
        }
    }
}
