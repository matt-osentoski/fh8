package org.jaroof.fh8.population.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class KafkaConsumerTimeService {

    private PopulationListService populationListService;

    public KafkaConsumerTimeService(PopulationListService populationListService) {
        this.populationListService = populationListService;
    }

    @KafkaListener(topics = "#{'${kafka.topic.time.epoch}'.split(',')}")
    public void consume(String message) {
        log.info(String.format("$$ -> Consumed Message -> %s",message));
        long epoch = Long.parseLong(message);
        populationListService.processLivingPopulation(epoch);
    }

}
